<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/20/2017
 * Time: 9:13 AM
 */
return [
    'title'=>[
        'delete'=>'Bạn có chắc muốn xóa bản ghi này ?',
        'multiDel'=>'Bạn có chắc muốn xóa những bản ghi đã chọn ?'
    ],
    'success' => [
        'add' => 'Tạo mới thành công.',
        'update' => 'Cập nhật thành công.',
        'delete' => 'Xóa thành công.'
    ],
    'error' => [
        'delete' => 'Xóa không thành công.'
    ]
];