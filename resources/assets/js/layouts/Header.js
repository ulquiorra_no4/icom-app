import React from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import API_URL from '../config/Config'
class Header extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reload : false,
            locale : 'VI',
            menu : [
                    {title: "Trang chủ",link: "/"},
                    {title: "Về chúng tôi",link: "/about"},
                    {title: "Dịch vụ",link: "/services"},
                    {title: "Tin tức",link: "/news"},
                    {title: "Tuyển dụng",link: "/requirement"},
                    {title: "Liên hệ",link: "/contact"}
                ],
            scrollY : 0,
            last_current : 0,
            isHide : false,
            isOpen : false,
            active : ''
        }
        this._getLocale()
        this._getMenu()
        this._toggleLocale = this._toggleLocale.bind(this)
        this.handleScroll = this.handleScroll.bind(this)
        this.handleMenuToggle = this.handleMenuToggle.bind(this)
    }
    handleScroll(event) {
        let target = event.target || event.srcElement
        let scrollTop = event.target.body.scrollTop,
            scrollY = window.scrollY;
        this.setState({
            scrollY : scrollY
        })
        if(scrollY>300){
            if((scrollY > this.state.last_current)&&(scrollY - this.state.last_current > 1)){
                this.setState({
                    last_current : scrollY,
                    isHide : true
                })
            }else if ((this.state.last_current > scrollY)&&(this.state.last_current - scrollY > 2)) {
                this.setState({
                    last_current : scrollY,
                    isHide : false
                })
            }
        }
    }
    handleMenuToggle(event){
        this.setState({
            isOpen : !this.state.isOpen
        })
    }
    componentDidMount(){

        this.setState({reload:!this.state.reload,active:window.location.href.split('/')[3]})
        window.addEventListener('scroll', this.handleScroll)
        // console.log(window.location.href.split('/'));
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll)
    }
    _getLocale(){
        axios.get(API_URL.get_locale)
        .then(response => {
            this.setState({
                locale : response.data,
                reload : !this.state.reload
            })
          })
          .catch(function (error) {
            console.log(error)
          });
    }
    _toggleLocale(e){
        let locale = e ? 'vi' : 'en';
        const data = {
            locale : locale
        }
        axios.post(API_URL.set_locale,data)
        .then(response => {
            this.setState({
                reload : !this.state.reload,
                locale : response.data
            })
            window.location.reload(true)
          })
          .catch(function (error) {
            console.log(error)
          });

    }

    _getMenu(){
        axios.get(API_URL.get_menu)
        .then(response => {
            this.setState({
                reload : !this.state.reload,
                menu: response.data
            })
          })
          .catch(function (error) {
            console.log(error)
          });
    }

    render(){
        let hiClass = ''
        if(this.state.scrollY < 300){
            hiClass = ''
        }else if (this.state.isHide) {
            hiClass = 'hidden-header'
        }else {
            hiClass = 'show-header'
        }
        let menuItem = <li></li>
        let dataMenu = this.state.menu
        let act = '/'+this.state.active
        if(dataMenu){
            menuItem = dataMenu.map(
                (obj,i) => <li key={i} className={obj.link==act?'active':''}><a href={obj.link} title={obj.title}>{obj.title}</a></li>
            )
        }
        return(
            <header className={hiClass}>
                <div className="container">
                    <div className="row">
                        <div className="col-md-2 logo">
                            <h1><a href="/" title="i-com.vn"><img src="/images/logo_1.png" alt="i-com.vn" /></a></h1>
                        </div>
                        <div className={this.state.isOpen?'col-md-10 navbar open':'col-md-10 navbar'}>
                            <nav>
                                <button className="btn" id="toggle_menu" onClick={this.handleMenuToggle.bind(this)}><span className="fa fa-bars"></span></button>
                                <ul className="top-menu">
                                    {menuItem}
                                    <li className="locale"><span id="lg_txt">{this.state.locale}</span><ul><li onClick={this._toggleLocale.bind(this,1)}><span>VI</span></li><li onClick={this._toggleLocale.bind(this,0)}><span>EN</span></li></ul></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header
