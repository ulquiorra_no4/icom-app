import React from 'react'
import axios from 'axios'
import API_URL from '../config/Config'
class Footer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reload : false,
            menu : [
                    {title: "Trang chủ",link: "/"},
                    {title: "Về chúng tôi",link: "/about"},
                    {title: "Dịch vụ",link: "/services"},
                    {title: "Tin tức",link: "/news"},
                    {title: "Tuyển dụng",link: "/requirement"},
                    {title: "Liên hệ",link: "/contact"}
                ],
            dataConfig : null
        }
        this._getMenu()
        this._getConfig()
    }
    _getMenu(){
        axios.get(API_URL.get_menu)
        .then(response => {
            this.setState({
                reload : !this.state.reload,
                menu: response.data
            })
          })
          .catch(function (error) {
            console.log(error)
          });
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'config')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    fetchMenu(){
        if (this.state.menu) {
          return this.state.menu.map( (obj, i) => {
            return <li key={i}><a href={obj.link} title={obj.title}><span className="fa fa-chevron-right"></span>{obj.title}</a></li>
          })
        }
    }
    render(){
        let menuItem = <li></li>
        let dataMenu = this.state.menu
        if(dataMenu){
            menuItem = dataMenu.map(
                (obj,i) => <li key={i}><a href={obj.link} title={obj.title}><span className="fa fa-chevron-right"></span>{obj.title}</a></li>
            )
        }
        let f_info = this.state.dataConfig
        let Comp = <div></div>
        let Expl = <div></div>
        let Conn = <div></div>
        if(f_info){
            Comp = <div>
                <h3>{f_info.company.name}</h3>
                <p><span className="fa fa-map-marker-alt"></span>{f_info.company.addr}</p>
                <p><span className="fa fa-phone"></span>{f_info.company.phone}</p>
                <p><span className="fa fa-envelope"></span>{f_info.company.email}</p>
            </div>
            Expl = <h3 className="hd">{f_info.menu.title}</h3>
            Conn = <h3 className="hd">{f_info.connect.title}</h3>
        }
        return(
            <footer id="footer">
                <div className="container">
                    <div className="row margin-20b">
                        <div className="col-md-5">
                            {Comp}
                        </div>
                        <div className="col-md-2">
                            {Expl}
                            <ul>
                                {menuItem}
                            </ul>
                        </div>
                        <div className="col-md-4 col-md-offset-1">
                            <h3 className="hd">Connect us</h3>
                            <ul className="social">
                                <li><a href="javascript:;" title=""><span className="soci sc-fa"></span></a></li>
                                <li><a href="javascript:;" title=""><span className="soci sc-gp"></span></a></li>
                                <li><a href="javascript:;" title=""><span className="soci sc-tw"></span></a></li>
                                <li><a href="javascript:;" title=""><span className="soci sc-tm"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xs-12 txt-center">
                        <p className="copy">Copyright © 2018. All rights reserved.</p>
                    </div>
                </div>

            </footer>
        )
    }
}

export default Footer
