const API_SERVER = 'http://172.16.40.190:8000/api'

const API_URL = {
    config_app : API_SERVER + '/get_config/',
    get_locale : API_SERVER + '/get_lang',
    set_locale : API_SERVER + '/set_lang',
    get_menu : API_SERVER + '/get_conf_menu',
    news_info : API_SERVER + '/news_page_info',
    news_list : API_SERVER + '/get_news/',
    news_detail : API_SERVER + '/news_detail/',
    news_side : API_SERVER + '/get_side_news',
    requirement_info : API_SERVER + '/requirement_page_info',
    requirement_list : API_SERVER + '/get_requirement/',
    requirement_detail : API_SERVER + '/requirement_detail/',
    service_info : API_SERVER + '/get_services',
    service_list : API_SERVER + '/get_services',
    service_detail : API_SERVER + '/ser_detail/',
    search_news : API_SERVER + '/search_news/',
    search_requirement : API_SERVER + '/search_requirement/'

}
export default API_URL
