export const gen_service_link = (slug) => {
    if(!slug) return '/';
    let title = slug.toLowerCase();
    title = title.replace(/\s/g, '-');
    return '/services/'+ title;
}

export const gen_news_link = (slug) => {
    if(!slug) return '/';
    let title = slug.toLowerCase();
    title = title.replace(/\s/g, '-');
    return '/news/'+ title;
}

export const gen_requirement_link = (slug) => {
    if(!slug) return '/';
    let title = slug.toLowerCase();
    title = title.replace(/\s/g, '-');
    return '/requirement/'+ title;
}
