import React from 'react';
import ReactDom from 'react-dom';
import Home from './components/home';
import News from './components/news';
import About from './components/about';
import Services from './components/services';
import ServiceDetail from './components/services/ServiceDetail';
import NewsDetail from './components/news/NewsDetail';
import Contact from './components/contact';
import Requirement from './components/requirement';
import RequirementDetail from './components/requirement/RequirementDetail';
import Header from './layouts/Header';
import Footer from './layouts/Footer';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
 ReactDom.render(
     <BrowserRouter>
         <div>
             <Header />
             <Switch>
                 <Route exact path="/" component={Home}></Route>
                 <Route exact path="/news" component={News}></Route>
                 <Route exact path="/news/:params" component={NewsDetail}></Route>
                 <Route exact path="/services" component={Services}></Route>
                 <Route exact path="/services/:params" component={ServiceDetail}></Route>
                 <Route exact path="/contact" component={Contact}></Route>
                 <Route exact path="/about" component={About}></Route>
                 <Route exact path="/requirement" component={Requirement}></Route>
                 <Route exact path="/requirement/:params" component={RequirementDetail}></Route>
             </Switch>
             <Footer />
         </div>
     </BrowserRouter>,
   document.getElementById('main')
 );
