import React from 'react'

import PropTypes from 'prop-types';

const ButtonLink = (props)=>(
    <a href={props.href} title={props.title} className="s-btn"><span className="fa fa-minus"></span>{props.name}</a>
)
ButtonLink.propTypes = {
    href : PropTypes.string,
    title : PropTypes.string,
    name : PropTypes.string
}
ButtonLink.defaultProps = {
    href : 'javascript:;'
}
export default ButtonLink
