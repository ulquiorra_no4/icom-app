import React from 'react'
import PropTypes from 'prop-types';
const images = {
    LikeHand : "icon-like-hand",
    Diamond : "icon ic-diamond",
    Idea : "icon ic-idea",
    Users : "icon ic-users",
    Target : "icon ic-target",
    Bars : "icon ic-bars",
    Television : "icon ic-television",
    Wallet : "icon ic-wallet"
}

const Icon = (props) => {
    let source = (props.name in images) ? images[props.name] : '';
    
    return(
        <span className={source}></span>
    )
}

Icon.propTypes = {
    name : PropTypes.string
}
export default Icon
