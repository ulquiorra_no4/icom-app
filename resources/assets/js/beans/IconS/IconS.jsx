import React from 'react'
import PropTypes from 'prop-types';
const images = {
    LikeHand : "icon-like-hand",
    Diamond : "icon ic-diamond",
    Idea : "icon ic-idea",
    Users : "icon ic-users",
    Target : "icon ic-target",
    Bars : "icon ic-bars",
    Television : "icon ic-television",
    Wallet : "icon ic-wallet",
    Shield : "icon ic-shield",
    Monitor : "icon ic-monitor",
    Light : "icon ic-light"
}

const IconS = (props) => {
    let source = ''
    switch (props.type) {
        case 1:
            source = "icon ic-television";
            break;
        case 2:
            source = "icon ic-wallet";
            break;
        case 3:
            source = "icon ic-shield";
            break;
        case 4:
            source = "icon ic-monitor";
            break;
        case 5:
            source = "icon ic-light";
            break;
        default:
            source = "icon ic-bars";
            break;
    }
    return(
        <span className={source}></span>
    )
}

IconS.propTypes = {
    
}
export default IconS
