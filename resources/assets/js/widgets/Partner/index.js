import React from 'react'
import Slider from "react-slick";
const prevArrow = <button><span className="fa fa-chevron-left"></span></button>;
const nextArrow = <button><span className="fa fa-chevron-right"></span></button>;
const LikeHand = ()=>(
    <span className="icon-like-hand"></span>
)
const Partner = (props)=>{
    let settings = {
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        initialSlide: 0,
        prevArrow,
        nextArrow,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 2,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          }
        ]
    }
    let partners = [
        {link:'/',thumbnail:'/pictures/manuf_mobi.png',title:''},
        {link:'/',thumbnail:'/pictures/manuf_vina.png',title:''},
        {link:'/',thumbnail:'/pictures/manuf_vtc.png',title:''},
        {link:'/',thumbnail:'/pictures/manuf_vtv.png',title:''},
        {link:'/',thumbnail:'/pictures/manuf_vina.png',title:''},
        {link:'/',thumbnail:'/pictures/manuf_vtc.png',title:''}
    ]
    let Item = partners.map(
        (item,k)=><div className="col-md-3 item" key={k}>
            <a href={item.link} title={item.title}><img src={item.thumbnail} /></a>
        </div>
    )
    return(
        <div id="cs_box">
            <div className="container">
                <div>
                    <LikeHand />
                    <div className="heading margin-30b">
                        <h3>{props.title}</h3>
                        <span>{props.description}</span>
                    </div>
                    <div className="row">
                        <Slider {...settings}>
                            {Item}
                        </Slider>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Partner
