import React from 'react'
import PropTypes from 'prop-types'
const Mid = (props) => {

    let htmlBrek = ''
    if(props.brekum){
        htmlBrek = props.brekum.map((item,i)=><li key={i}>{item.link?<a href={item.link} title={item.title}>{item.title}</a>:<span>{item.title}</span>}</li>)
    }
    return(
        <div className="mid-banner relative">
            <div className="thumber">
                <img src={props.thumbnail} alt={props.name} />
            </div>
            <div className="shadow">
                <div className="container">
                    <div className="inner">
                        <h3 className="margin-30b">{props.name} <span className="line-end"></span></h3>
                        <ul className="brekum">
                            {htmlBrek}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
Mid.propTypes = {
    thumbnail: PropTypes.string,
    name: PropTypes.string,
}
Mid.defaultProps = {
    thumbnail: '/pictures/banner_about.jpg',
    brekum: []
}
export default Mid
