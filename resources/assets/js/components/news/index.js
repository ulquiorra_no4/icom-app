import React from 'react';
import Mid from '../../widgets/Mid';
import {Icon, ButtonLink} from '../../beans';
import {gen_news_link} from '../../helpers/Helpers'
import axios from 'axios'
import API_URL from '../../config/Config'
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data : null,
            reload : false,
            titlePage : 'Tin tức Icom',
            page : 1,
            total : 0,
            sideBar : null,
            dataMenu : null,
            keyword : ''
        }
        this._getInfo()
        this.handleClick = this.handleClick.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this._getData(this.state.page)
        this._getSideBar()
        this._getMenu()
    }
    handleClick(e){
        this.setState({ page: e });
        this._getData(e)
    }
    handleSubmit(event){
        event.preventDefault()
        this._getData(this.state.page)
    }
    handleChange(event) {
        this.setState({keyword: event.target.value});
    }
    componentDidMount(){
        this.setState({reload:!this.state.reload});
        document.title = this.state.titlePage;
    }
    _getInfo(){
        axios.get(API_URL.news_info)
        .then(res=>{
            this.setState({
                titlePage : res.data.banner.title,
                reload : !this.state.reload
            })
        })
        .catch(function(er){console.log(er);})
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getData(page){

        let api_url = this.state.keyword ? API_URL.search_news + this.state.keyword + '/' + page : API_URL.news_list + page

        axios.get(api_url)
        .then(res=>{
            this.setState({
                reload : !this.state.reload,
                data : res.data.data,
                total : res.data.total
            })
            // console.log(res.data);
        })
        .catch(function(er){console.log(er);})
    }
    _getSideBar(){
        axios.get(API_URL.news_side)
        .then(
            res=>{
                this.setState({
                    reload : !this.state.reload,
                    sideBar : res.data
                })
            }
        )
        .catch(function(er){console.log(er);})
    }
    _sideView(){
        let htmlLatest = '';
        let htmlView = '';
        if(this.state.sideBar){
            let latest = this.state.sideBar.latest
            let viewest = this.state.sideBar.view
            htmlLatest = latest.map(
                (item,i) => <li key={i}><a href={gen_news_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
            htmlView = viewest.map(
                (item,i) => <li key={i}><a href={gen_news_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
        }
        return(
            <aside className="col-md-4">
                <div className="row-box search-box">
                    <form>
                        <input type="text" name="q" placeholder="search" value={this.state.keyword} onChange={this.handleChange} />
                        <button onClick={this.handleSubmit}><span className="fa fa-search"></span></button>
                    </form>
                </div>
                <div className="row-box cat-box">
                    <h3>Chuyên mục <span className="line-end"></span></h3>
                    <ul>
                        <li className="pr">
                            <a href="javascript:;">Thông tin mới nhất</a>
                            <ul>
                                {htmlLatest}
                            </ul>
                        </li>
                        <li className="pr">
                            <a href="javascript:;">Thông tin đọc nhiều nhất</a>
                            <ul>{htmlView}</ul>
                        </li>
                    </ul>
                </div>
            </aside>
        )
    }
    _contentView(){
        if(this.state.data){
            let post = this.state.data
            let Post = post.map(
                (item,k) =>
                <li key={k}>
                    <div className="item">
                        <a href={gen_news_link(item.slug)} title={item.title} className="thumb"><img src={'http://i-com.vn/uploads/pictures/news/'+item.avatar} alt={item.title}/></a>
                        <span className="timeup">{item.published_at}</span>
                        <a href={gen_news_link(item.slug)} title={item.title} className="title">{item.title}</a>
                        <p>{item.title}</p>
                        <ButtonLink href={gen_news_link(item.slug)} name="Tìm hiểu thêm" title={item.title}/>
                    </div>
                </li>
            )
            return(
                <article className="col-md-8">
                    <ul className="news-feed">
                        {Post}
                    </ul>
                </article>
            )
        }

    }
    _paginationView(){
        if(this.state.total && Math.ceil(this.state.total / 5) >1){
            let pagiItem = [];
            for(let i=1;i<=Math.ceil(this.state.total / 5);i++){
                pagiItem.push(i)
            }
            let Item = pagiItem.map(num=><li key={num}><a href="javascript:;" onClick={this.handleClick.bind(this,num)}>{num}</a></li>)

            return(
                <div>
                    <ul className="pagination">
                        {Item}
                    </ul>
                </div>
            )
        }

    }
    render(){
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        return(
            <section>
                <Mid name={this.state.titlePage} brekum={brekum} />
                <div id="content_detail">
                    <div className="container">
                        <div className="row">
                            {this._contentView()}
                            {this._sideView()}
                        </div>
                        {this._paginationView()}
                    </div>
                </div>
            </section>
        )
    }
}

export default Index
