import React from 'react'
import Mid from '../../widgets/Mid'
import {gen_news_link} from '../../helpers/Helpers'
import axios from 'axios'
import API_URL from '../../config/Config'
class NewsDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null,
            dataRel : null,
            sideBar : null,
            reload : false,
            titlePage : props.match.params.params,
            slug : props.match.params.params,
            isOpen1 : false,
            isOpen2 : false,
            dataMenu : null,
            keyword : '',
            dataSugg : null
        }
        this._getData()
        this._getSideBar()
        this._getMenu()
        this._handleClickBar = this._handleClickBar.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

    }
    _handleClickBar(event){
        switch (event) {
            case 2:
            this.setState({
                isOpen1 : false,
                isOpen2 : !this.state.isOpen2
            });
                break;
            default:
            this.setState({
                isOpen2 : false,
                isOpen1 : !this.state.isOpen1
            })
        }

    }
    handleSubmit(event){
        event.preventDefault()
        this._getSearch(this.state.page)
    }
    handleChange(event) {
        this.setState({keyword: event.target.value})
        this._getSearch(this.state.page)
        console.log(this.state.keyword);
    }
    _getSearch(){
        let api_url = API_URL.search_news + this.state.keyword + '/1'
        setTimeout(function(){
            axios.get(api_url)
            .then(
                res=>{
                    this.setState({
                        dataSugg : res.data.data
                    })
                }
            )
            .catch(function(er){console.log(er);})
        }.bind(this),2000)

    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getData(){
        axios.get(API_URL.news_detail + this.state.slug)
        .then(
            res=>{
                this.setState({
                    reload : !this.state.reload,
                    data : res.data.detail,
                    dataRel : res.data.relation,
                    titlePage : res.data.detail.title
                })
            }
        )
        .catch(function(er){console.log(er);})
    }
    _getSideBar(){
        axios.get(API_URL.news_side)
        .then(
            res=>{
                this.setState({
                    reload : !this.state.reload,
                    sideBar : res.data
                })
            }
        )
        .catch(function(er){console.log(er);})
    }
    componentDidMount(){
        this.setState({
            reload:!this.state.reload
        })
        document.title = this.state.titlePage;
    }
    _sideView(){
        let htmlRel = '';
        if(this.state.dataRel){
            let rel = this.state.dataRel
            htmlRel = rel.map(
                (item,i) => <li key={i}>
                    <a href={gen_news_link(item.slug)} title={item.title}><img src={'http://i-com.vn/uploads/pictures/news/'+item.avatar} alt={item.title} />{item.title}</a>
                </li>
            )
        }
        let htmlLatest = '';
        let htmlView = '';
        let suggItem = ''
        if(this.state.sideBar){
            let latest = this.state.sideBar.latest
            let viewest = this.state.sideBar.view
            htmlLatest = latest.map(
                (item,i) => <li key={i}><a href={gen_news_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
            htmlView = viewest.map(
                (item,i) => <li key={i}><a href={gen_news_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
        }
        if(this.state.dataSugg){
            suggItem = this.state.dataSugg.map(
                (item,i) => <li key={i}><a href={gen_news_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
        }
        return(
            <aside className="col-md-4">
                <div className="row-box search-box">
                    <form>
                        <input type="text" value={this.state.keyword} name="q" placeholder="search" autoComplete="off" onChange={this.handleChange} />
                        <button onClick={this.handleSubmit}><span className="fa fa-search"></span></button>
                        <ul id="sugg_box" className={this.state.keyword?'':'none'}>
                            {suggItem}
                        </ul>
                    </form>
                </div>
                <div className="row-box cat-box">
                    <h3>Chuyên mục <span className="line-end"></span></h3>
                    <ul>
                        <li className={this.state.isOpen1?'open pr':'pr'}>
                            <a href="javascript:;" onClick={this._handleClickBar.bind(this,1)}>Thông tin mới nhất</a>
                            <ul>
                                {htmlLatest}
                            </ul>
                        </li>
                        <li className={this.state.isOpen2?'open pr':'pr'}>
                            <a href="javascript:;" onClick={this._handleClickBar.bind(this,2)}>Thông tin đọc nhiều nhất</a>
                            <ul>{htmlView}</ul>
                        </li>
                    </ul>
                </div>
                <div className="row-box rel-box">
                    <h3>Tin liên quan <span className="line-end"></span></h3>
                    <ul>
                        {htmlRel}
                    </ul>
                </div>
            </aside>
        )
    }
    _contentView(){
        let html = '';
        if(this.state.data){
            let detail = this.state.data
            html = <div className="content-detail">
                <div className="thumb row-box">
                    <img src={'http://i-com.vn/uploads/pictures/news/'+detail.avatar} alt={detail.title} />
                </div>
                <span className="timeup">{detail.published_at}</span>
                <h1>{detail.title}</h1>
                <div dangerouslySetInnerHTML={{__html: detail.content}} />
            </div>
        }
        return(
            <article className="col-md-8">
                {html}
            </article>
        )
    }
    render(){
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : 'Tin tức',
                link : '/news'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        return(
            <section>
                <Mid name="Tin tức" brekum={brekum} />
                <div id="content_detail">
                    <div className="container">
                        <div className="row">
                            {this._contentView()}
                            {this._sideView()}
                        </div>
                    </div>
                    <div className="clear"></div>
                </div>
            </section>
        )
    }
}

export default NewsDetail
