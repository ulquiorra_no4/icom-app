import React from 'react';
import Slider from "react-slick";
import Partner from '../../widgets/Partner';
import {Icon,IconS,ButtonLink} from '../../beans';
import axios from 'axios'
import API_URL from '../../config/Config'
import {gen_news_link,gen_service_link} from '../../helpers/Helpers'
class Index extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data : null,
            dataServices : null,
            reload : false,
            dataConfig : null,
            dataButton : null,
            transform : 0,
            ani1 : false,
            ani2 : false,
            ani3 : false,
            ani4 : false,
            anic1 : false,
            anic2 : false,
            anis1 : false,
            anis2 : false,
            anin : false,
            anit : false
        }
        this._getNews()
        this._getConfig()
        this._getService()
        this._getButton()
        this.handleScroll = this.handleScroll.bind(this)
    }
    componentDidMount(){
        document.title = 'Công ty CP Dịch vụ Truyền thông VietNamNet Icom';
        this.setState({reload:!this.state.reload})
        window.addEventListener('scroll', this.handleScroll);
        setTimeout(function() { this.setState({ani4: true}); }.bind(this), 1000);
        setTimeout(function() { this.setState({ani1: true}); }.bind(this), 600);
        setTimeout(function() { this.setState({ani2: true}); }.bind(this), 1200);
        setTimeout(function() { this.setState({ani3: true}); }.bind(this), 1000);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        let scrollTop = event.target.body.scrollTop,
            itemTranslate = window.scrollY;
            this.setState({
                transform : itemTranslate,
                reload : !this.state.reload
            })
            if(this.state.transform > 190){
                this.setState({anic1: true})
                // setTimeout(function() { this.setState({anic1: true}); }.bind(this), 600);
            }
            if(this.state.transform > 320){
                this.setState({anic1: true})
                this.setState({anic2: true})
            }
            if(this.state.transform > 840){
                this.setState({anis1: true})
                this.setState({anis2: true})
            }
            if(this.state.transform > 1350){
                this.setState({anin: true})
            }
            if(this.state.transform > 1840){
                this.setState({anit: true})
            }
            // console.log(this.state.transform);
    }
    _getButton(){
        axios.get(API_URL.config_app + 'button')
        .then(
            res => {
                this.setState({
                    dataButton : res.data,
                    reload : !this.state.reload
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getNews(){
        axios.get(API_URL.news_list + 1)
        .then(
            res => {
                this.setState({
                    data : res.data.data.slice(-5, -2),
                    reload : !this.state.reload
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'homepage')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getService(){
        axios.get(API_URL.service_list)
        .then(res=>{
                this.setState({
                    dataServices : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        ).catch(function (er) {
          console.log(er)
        });
    }
    _midView(){
        let thumbClass = ['thumb-animate']
        let tbc1 = ['thumber a']
        let tbc2 = ['a hidden-xs']
        let tbc3 = ['a']
        let tbc4 = ['thumb-animate hidden-xs a']
        // console.log(this.state.ani4);
        if(this.state.ani4){
            tbc4.push('animated','fadeInRightBig')
        }
        if(this.state.ani1){
            tbc1.push('animated','fadeInLeftBig')
        }
        if(this.state.ani2){
            tbc2.push('animated','fadeInUp')
        }
        if(this.state.ani3){
            tbc3.push('animated','fadeIn')
        }
        let htmlView = <div></div>
        if(this.state.dataConfig && this.state.dataButton){
            let data = this.state.dataConfig
            let butt = this.state.dataButton
            htmlView = <div className="inner">
                <div className={tbc3.join(' ')}>
                    <h2 className="title-cap">{data.banner.title}</h2>
                    <p className="sapo">{data.banner.description}</p>
                </div>
                <div className={tbc2.join(' ')}><ButtonLink href="/about" title={butt.more} name={butt.more} /></div>
            </div>
        }
        return(
            <div className="mid-banner relative">
                <div className={tbc1.join(' ')}>
                    <img src="/pictures/main-bg.jpg" alt="Home" />
                </div>
                <div className="shadow">
                    <div className="container">
                        {htmlView}
                    </div>
                </div>
                <div className={tbc4.join(' ')}><img src="/pictures/iphone0.png" /></div>
            </div>
        )
    }
    _serviceView(){
        let settings = {
              infinite: false,
              speed: 500,
              slidesToShow: 3,
              slidesToScroll: 1,
              initialSlide: 0,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
        }
        let c_ani1 = ['a']
        let c_ani2 = ['a']
        let s_ani1 = ['a']
        let s_ani2 = ['a']
        if(this.state.anic1){
            c_ani1.push('animated','fadeInUp')
        }
        if(this.state.anic2){
            c_ani2.push('animated','fadeInUp')
        }
        if(this.state.anis1){
            s_ani1.push('animated','fadeInLeft')
        }
        if(this.state.anis2){
            s_ani2.push('animated','zoomIn')
        }

        let serTit = ''
        let serDes = ''
        let valItem = <div></div>
        let valTitle = ''
        if(this.state.dataConfig){
            serTit = this.state.dataConfig.service.title
            serDes = this.state.dataConfig.service.description
            valTitle = this.state.dataConfig.values.title
            let ic = ['Diamond','Idea','Users','Target']
            valItem = this.state.dataConfig.values.info.map(
                (item,k)=>
                <li key={k}>
                    <div className={c_ani1.join(' ')}>
                        <Icon name={ic[k]} />
                        <strong>{item.title}</strong>
                        <p>{item.description}</p>
                    </div>
                </li>
            )
        }

        let serv = this.state.dataServices;
        let slideItem = <div></div>
        if (serv) {
            slideItem = serv.map(
                (item,k)=><div className="slide-item" key={k}>
                    <div className={s_ani2.join(' ')}>
                        <a href={gen_service_link(item.slug)} title={item.title}>
                            <img src="/pictures/serv1.png" alt={item.title} />
                            <div className="shadow">
                                <IconS type={k} />
                                <strong>{item.title}</strong>
                                <p>{item.description}</p>
                            </div>
                        </a>
                    </div>
                </div>
            )
        }
        let butt = this.state.dataButton
        let Button = <div></div>
        if(butt){
            Button = <ButtonLink href="/services" title={butt.more} name={butt.more} />
        }
        return(
            <div id="_service">
                <div className="row-table">
                    <div className="row-box">
                        <div className="thumbleft col-md-6">
                            <div className={c_ani1.join(' ')}>
                                <img src="/pictures/thumbleft2.jpg" alt="service" />
                            </div>
                        </div>
                        <div className="inforight col-md-6">
                            <Icon name="LikeHand" />
                            <div className="heading">
                                <h3>{valTitle}</h3>
                            </div>
                            <ul className="list">
                                {valItem}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="row-table">
                    <div className="row-box margin-30b">
                        <div className="ct-left">
                            <div className={s_ani1.join(' ')}>
                                <img src="/pictures/thumbleft2.png" />
                                <div className="shadow">
                                    <div className="inner">
                                        <h2>{serTit}</h2>
                                        <p>{serDes}</p>
                                        {Button}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="sv_box" className="relative">
                            <div className="carousel">
                                <Slider {...settings}>
                                    {slideItem}
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            </div>
        )
    }
    _newsView(){
        let n_ani = ['a']
        if(this.state.anin){
            n_ani.push('animated','zoomIn')
        }
        let htmlNew = <div></div>
        if(this.state.data && this.state.dataButton){
            let news = this.state.data
            let butt = this.state.dataButton
            htmlNew = news.map((item,i)=>
            <div className="item col-md-4" key={i}>
                <div className={n_ani.join(' ')}>
                    <a href={gen_news_link(item.slug)} title={item.title} className="thumb"><img src={'http://i-com.vn/uploads/pictures/news/'+item.avatar} alt={item.title}/></a>
                    <div className="info">
                        <span className="time">{item.published_at}</span>
                        <a href={gen_news_link(item.slug)} title={item.title} className="title">{item.title}</a>
                        <p>{item.description}</p>
                        <ButtonLink href={gen_news_link(item.slug)} title={item.title} name={butt.more} />
                    </div>
                </div>
            </div>
            )
        }
        let conf = this.state.dataConfig
        let newsTit = <div></div>
        if(conf){
            newsTit = <h3>{conf.news.title}</h3>
        }

        return(
            <div id="news_box" className="margin-40b">
                <div className="container">
                    <div>
                        <Icon name="LikeHand" />
                        <div className="heading margin-30b">
                            {newsTit}
                        </div>
                        <div className="feed row">
                            {htmlNew}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    _iView(){
        let htmlView = <div></div>
        let t_ani = ['a i-top']
        if (this.state.anit) {
            t_ani.push('animated','zoomIn')
        }
        let dataTop = this.state.dataConfig
        if(dataTop){
            htmlView = dataTop.top.map(
                (obj,k)=><div className="col-md-4" key={k}>
                    <div className={t_ani.join(' ')}>
                        <span className="txt-color">{obj.title}</span>
                        <p>{obj.description}</p>
                    </div>
                </div>
            )
        }
        return(
            <div id="i_box" className="margin-30b">
                <div className="container">
                    <div className="row">
                        {htmlView}
                    </div>
                </div>
            </div>
        )
    }
    render(){
        let partner = <div></div>
        if(this.state.dataConfig){
            let cn = this.state.dataConfig.customer
            partner = <Partner title={cn.title} description={cn.description} />
        }
        return(
            <section>
                {this._midView()}
                {this._serviceView()}
                {this._newsView()}
                {this._iView()}
                {partner}
            </section>
        )
    }
}

export default Index
