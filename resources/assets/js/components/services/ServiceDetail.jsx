import React from 'react'
import Mid from '../../widgets/Mid'
import axios from 'axios'
import {gen_service_link} from '../../helpers/Helpers'
import API_URL from '../../config/Config'
class ServiceDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reload : false,
            titlePage : props.match.params.params,
            data : null,
            dataService : null,
            dataConfig : null,
            dataCate : null,
            dataMenu : null
        }
        this._getData()
        this._getServices()
        this._getConfig()
        this._getCate()
        this._getMenu()
    }
    componentDidMount(){
        this.setState({
            reload:!this.state.reload
        })
        document.title = this.state.titlePage;

    }
    _getServices(){
        axios.get(API_URL.service_list)
        .then(res=>{
                this.setState({
                    dataService : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        ).catch(function (er) {
          console.log(er)
        });
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'service')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getCate(){
        axios.get(API_URL.config_app + 'config')
        .then(
            res => {
                this.setState({
                    dataCate : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getData(){
        axios.get(API_URL.service_detail + this.state.titlePage)
        .then(res=>{
            this.setState({
                reload : !this.state.reload,
                data : res.data,
                titlePage : res.data.title
            })
            // console.log(res.data);
        })
        .catch(function(er){
            console.log(er);
        })
    }
    _sideView(){
        let serAll = this.state.dataService
        let htmlItem = <li></li>
        if (serAll) {
            htmlItem = serAll.map(
                (item,k)=><li key={k}><a href={gen_service_link(item.slug)} title={item.title}>{item.title}</a></li>
            )
        }
        let cate = this.state.dataCate
        let c_title = ''
        let c_addr = ''
        let c_phone = ''
        let c_email = ''
        let c_ser = ''
        if(cate){
            c_title = cate.cate.cont
            c_addr = cate.company.addr
            c_phone = cate.company.phone
            c_email = cate.company.email
            c_ser = cate.cate.service
        }
        return(
            <aside className="col-md-3">
                <div className="row-box margin-30b">
                    <div className="head-bar">
                        <h3>{c_ser}</h3>
                    </div>
                    <ul className="side-links">
                        {htmlItem}
                    </ul>
                </div>
                <div className="row-box margin-30b con-box">
                    <h3>{c_title} <span className="line-end"></span></h3>
                    <p>{c_addr}</p>
                    <p>{c_phone}</p>
                    <p>{c_email}</p>
                </div>
            </aside>
        )
    }
    _contentView(){
        // console.log(this.state.data);
        let content = '';
        if(this.state.data){
            let detail = this.state.data
            content =
                <div className="content-detail">
                    <div className="thumbnail">
                        <img src="" alt="" />
                    </div>
                    <div dangerouslySetInnerHTML={{__html: detail.content}} />
                </div>
        }
        return(
            <article className="col-md-9">
                {content}
            </article>
        )
    }
    render(){
        let conf = this.state.dataConfig
        let br_tit = ''
        if(conf){
            br_tit = conf.banner.title
        }
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : br_tit,
                link : '/requirement'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        return(
            <section>
                <Mid name="Dịch vụ của chúng tôi" brekum={brekum}/>
                <div id="content_detail">
                    <div className="container">
                        <div className="row">
                            {this._sideView()}
                            {this._contentView()}
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}


export default ServiceDetail
