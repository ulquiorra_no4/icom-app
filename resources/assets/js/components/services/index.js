import React from 'react'
import Mid from '../../widgets/Mid'
import axios from 'axios'
import {Icon,ButtonLink} from '../../beans'
import {gen_service_link} from '../../helpers/Helpers'
import API_URL from '../../config/Config'
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data : null,
            reload: false,
            titlePage : 'Dịch vụ của chúng tôi',
            dataConfig : null,
            dataButton : null,
            dataCf : null,
            dataMenu : null
        }
        this._getData()
        this._getConfig()
        this._getButton()
        this._getContact()
        this._getMenu()
    }
    componentDidMount(){
        document.title = this.state.titlePage
        this.setState({reload : !this.state.reload})
    }
    _getData(){
        axios.get(API_URL.service_list)
        .then(res=>{
                this.setState({
                    data : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        ).catch(function (er) {
          console.log(er)
        });
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'service')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload,
                    titlePage : res.data.banner.title
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getButton(){
        axios.get(API_URL.config_app + 'button')
        .then(
            res => {
                this.setState({
                    dataButton : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getContact(){
        axios.get(API_URL.config_app + 'config')
        .then(
            res => {
                this.setState({
                    dataCf : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _serView(){
        if(this.state.data){
            let data = this.state.data
            let Service = data.map(
                (item,k) =>
                <li key={k}>
                    <div className="child-item">
                        <a href={gen_service_link(item.slug)} title={item.title} className="title">{item.title}</a>
                        <p>{item.description}</p>
                    </div>
                </li>
            )

            return(
                <div id="serv_box">
                    <div className="container">
                        <div className="heading">
                            <h3>{this.state.titlePage}</h3>
                        </div>
                    </div>
                    <div className="content">
                        <div className="big-thumb hidden-xs">
                            <img src="/pictures/service_thumb.png"/>
                        </div>
                        <ul>
                            {Service}
                        </ul>
                    </div>
                </div>
            )
        }


    }
    _exView(){
        if(this.state.dataConfig){
            let data = this.state.dataConfig.top
            return(
                <div id="exp_box">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <a href="/" title="i-com.vn" className="logo2"><img src="/images/logo_1.png" alt="i-com.vn"/></a>
                                <h2>{data.title}</h2>
                                <p>{data.description}</p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }
    _conView(){
        let conf = this.state.dataCf
        let butt = this.state.dataButton
        let data = this.state.dataConfig
        let h3Tit = ''
        let Desc = ''
        let addr = ''
        let phone = ''
        let email = ''
        let Button = <div></div>
        if(data){
            h3Tit = data.contact.title
            Desc = data.contact.description
        }
        if(conf){
            addr = conf.company.addr
            phone = conf.company.phone
            email = conf.company.email
        }
        if(butt){
            Button = <ButtonLink href="/contact" title={butt.contact} name={butt.contact} />
        }
        return(
            <div id="cont_box">
                <div className="wrapper">
                    <div className="row">
                        <div className="col-md-6">
                            <img src="/pictures/iphone1.jpg" />
                        </div>
                        <div className="col-md-6">
                            <div className="heading margin-30b">
                                <h3>{h3Tit}</h3>
                                <p>{Desc}</p>
                            </div>
                            <ul>
                                <li><span className="fa fa-map-marker-alt"></span>{addr}</li>
                                <li><span className="fa fa-phone"></span>{phone}</li>
                                <li><span className="fa fa-envelope"></span>{email}</li>
                            </ul>
                            <div className="row-box">
                                {Button}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    render(){
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        return(
            <section>
                <Mid name={this.state.titlePage} brekum={brekum}/>
                {this._serView()}
                {this._exView()}
                {this._conView()}
            </section>
        )
    }
}

export default Index
