import React from 'react'
import Mid from '../../widgets/Mid'
import axios from 'axios'
import API_URL from '../../config/Config'
class RequirementDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            data : null,
            reload : false,
            titlePage : props.match.params.params,
            slug : props.match.params.params,
            dataMenu : null
        }
        this._getData()
        this._getMenu()
    }
    componentDidMount(){
        document.title = this.state.titlePage
        this.setState({
            reload : !this.state.reload
        })
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getData(){
        axios.get(API_URL.requirement_detail + this.state.slug)
        .then(
            res=>{
                this.setState({
                    reload : !this.state.reload,
                    data : res.data,
                    titlePage : res.data.title
                })
            }
        )
        .catch(function(er){console.log(er);})
    }
    render(){
        let html = '';
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        if(this.state.data){
            let detail = this.state.data
            html = <div className="content-detail">
                <h1>{detail.title}</h1>
                <div dangerouslySetInnerHTML={{__html: detail.content}} />
            </div>
        }
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : 'Tuyển dụng',
                link : '/requirement'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        return(
            <section>
                <Mid name="Tuyển dụng" brekum={brekum} />
                    <div id="content_detail">
                        <div className="container">
                            {html}
                        </div>
                    </div>
            </section>
        )
    }
}

export default RequirementDetail
