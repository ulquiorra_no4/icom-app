import React from 'react'
import Mid from '../../widgets/Mid'
import axios from 'axios'
import {gen_requirement_link} from '../../helpers/Helpers'
import API_URL from '../../config/Config'
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data : null,
            reload: false,
            titlePage : 'Tuyển dụng',
            page : 1,
            total : 0,
            dataConfig : null,
            dataMenu : null,
            keyword : ''
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this._getInfo()
        this._getConfig()
        this._getMenu()
        this._getData(this.state.page)
    }
    handleSubmit(event){
        event.preventDefault()
        this._getData(this.state.page)
    }
    handleChange(event) {
        this.setState({keyword: event.target.value});
    }
    componentDidMount(){
        document.title = this.state.titlePage;
        this.setState({reload:!this.state.reload})
    }
    _getInfo(){
        axios.get(API_URL.requirement_info)
        .then(res=>{
            this.setState({
                titlePage : res.data.banner.title,
                reload : !this.state.reload
            })
        })
        .catch(function(er){console.log(er);})
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'requirement')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload,
                    titlePage : res.data.banner.title
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getData(page){

        let api_url = this.state.keyword ? API_URL.search_requirement + this.state.keyword : API_URL.requirement_list + page
        axios.get(api_url)
        .then(res=>{
            this.setState({
                reload : !this.state.reload,
                data : res.data.data,
                total : res.data.total
            })
        })
        .catch(function(er){console.log(er);})
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    render(){

        let htmlItem = <tr></tr>
        if (this.state.data) {
            let posts = this.state.data
            htmlItem = posts.map(
                (item,i) => <tr key={i}>
                    <td>{i+1}</td>
                    <td>{item.published_at}</td>
                    <td style={stylesheet.textLeft}><a href={gen_requirement_link(item.slug)} title={item.title}>{item.title}</a></td>
                    <td>{item.place?item.place:'Hà Nội'}</td>
                    <td>{item.salary?item.salary:'Thỏa thuận'}</td>
                </tr>
            )
        }
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        let data = this.state.dataConfig
        let title = ''
        if(data){
            title = data.form.title
        }
        return(
            <section>
                <Mid name={this.state.titlePage} brekum={brekum} />
                <div id="content_detail">
                    <div className="container">
                        <div className="row job-box margin-30b">
                            <div className="col-md-6 hd">
                                <h2>{title} <span className="line-end"></span></h2>
                            </div>
                            <div className="col-md-6">
                                <div className="find-job row-box">
                                    <form>
                                        <input name="job" type="text" placeholder="Search" value={this.state.keyword} onChange={this.handleChange} />
                                        <button onClick={this.handleSubmit}><span className="fa fa-search"></span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="module-job">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>TT</th>
                                        <th>Ngày đăng</th>
                                        <th style={stylesheet.textLeft}>Công việc</th>
                                        <th>Địa điểm</th>
                                        <th>Mức lương</th>
                                    </tr>
                                </thead>
                                <tbody>{htmlItem}</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const stylesheet = {
    textLeft : {
        textAlign : "left"
    }
}
export default Index
