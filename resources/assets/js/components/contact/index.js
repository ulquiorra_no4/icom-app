import React from 'react'
import {Mid} from '../../widgets'
import API_URL from '../../config/Config'
import axios from 'axios'
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data : null,
            reload: false,
            titlePage : 'Liên hệ với chúng tôi',
            dataConfig:null,
            dataCf :null,
            dataMenu : null
        }
        this._getConfig()
        this._getContact()
        this._getMenu()
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'contact')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    reload : !this.state.reload,
                    titlePage : res.data.banner.title
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    _getContact(){
        axios.get(API_URL.config_app + 'config')
        .then(
            res => {
                this.setState({
                    dataCf : res.data,
                    reload : !this.state.reload,
                })
                // console.log(res.data);
            }
        )
        .catch(function(er){console.log(er)})
    }
    componentDidMount(){
        document.title = this.state.titlePage;
        this.setState({reload:!this.state.reload})
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    render(){
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum = [
            {
                title : homep,
                link : '/'
            },
            {
                title : this.state.titlePage,
                link : ''
            }
        ]
        let data = this.state.dataConfig
        let title = ''
        let desc = ''
        let frm_name = ''
        let frm_phone = ''
        let frm_email = ''
        let frm_com = ''
        let frm_mess = ''
        let txt_but = ''
        let Comp = <div></div>
        if(data){
            title = data.form.title
            desc = data.form.description
            frm_name = data.form.label.name
            frm_phone = data.form.label.phone
            frm_email = data.form.label.email
            frm_com = data.form.label.company
            frm_mess = data.form.label.message
            txt_but = data.form.label.button
        }
        let conf = this.state.dataCf
        if(conf){
            Comp = <div>
                <p>{conf.company.addr}</p>
                <p>{conf.company.phone}</p>
                <p>{conf.company.email}</p>
            </div>
        }
        return(
            <section>
                <Mid name={this.state.titlePage} thumbnail="/pictures/banner_contact.jpg" brekum={brekum} />
                <div id="content_detail">
                    <div className="container">
                        <div className="col-xs-12 hd margin-30b">
                            <h2>{title} <span className="line-end"></span></h2>
                            <p>{desc}</p>
                        </div>
                        <div className="row">
                            <div className="col-md-9">
                                <form action="" method="post" id="frm_contact">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ct_name">{frm_name} <span>*</span></label>
                                            <input type="text" name="ct_name" id="ct_name" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ct_phone">{frm_phone} <span>*</span></label>
                                            <input type="text" name="ct_phone" id="ct_phone" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ct_mail">{frm_email} <span>*</span></label>
                                            <input type="email" name="ct_mail" id="ct_mail" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="ct_company">{frm_com} </label>
                                            <input type="text" name="ct_company" id="ct_company" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label htmlFor="ct_mess">{frm_mess} <span>*</span></label>
                                            <textarea name="ct_mess" rows="8" id="ct_mess" className="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <button className="s-btn"><span className="fa fa-minus"></span> {txt_but}</button>
                                    </div>
                                </form>
                            </div>
                            <div className="col-md-3 c-box">
                                <div className="row-box margin-30b con-box">
                                    {Comp}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Index
