import React from 'react'
import Slider from "react-slick"
import Mid from '../../widgets/Mid'
import Partner from '../../widgets/Partner'
import axios from 'axios'
import API_URL from '../../config/Config'
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataConfig : null,
            data : null,
            reload: false,
            transform : 0,
            titlePage : 'Về chúng tôi',
            anih : true,
            anim : false,
            dataMenu : null
        }
        this._getConfig()
        this._getMenu()
        this.handleScroll = this.handleScroll.bind(this)
    }
    componentDidMount(){
        document.title = this.state.titlePage
        this.setState({reload:!this.state.reload})
        window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    _getMenu(){
        axios.get(API_URL.config_app + 'menu')
        .then(
            res => {
                this.setState({
                    dataMenu : res.data,
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    handleScroll(event) {
        let scrollTop = event.target.body.scrollTop,
            itemTranslate = window.scrollY
            this.setState({
                transform : itemTranslate
            })
            if(this.state.transform > 200){
                setTimeout(function() { this.setState({anim: true}); }.bind(this), 200)
            }
            // if(this.state.transform > 1350){
            //     setTimeout(function() { this.setState({anin: true}); }.bind(this), 200);
            // }
            // if(this.state.transform > 1840){
            //     setTimeout(function() { this.setState({anit: true}); }.bind(this), 200);
            // }
    }
    _getConfig(){
        axios.get(API_URL.config_app + 'about')
        .then(
            res => {
                this.setState({
                    dataConfig : res.data,
                    titlePage : res.data.banner.title,
                    reload : !this.state.reload
                })
            }
        )
        .catch(function(er){console.log(er)})
    }
    _midView(){
        return(
            <div className="mid-banner relative">
                <div className="thumber">
                    <img src="/pictures/banner_about.jpg" alt="About" />
                </div>
                <div className="shadow">
                    <div className="container">
                        <div className="inner">
                            <h3 className="margin-30b">Về chúng tôi <span className="line-end"></span></h3>
                            <ul className="brekum">
                                <li><a href="/" title="home">home</a></li>
                                <li><span>about</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    _hisView(){
        let h_ani = ['a']
        if(this.state.anih){
            h_ani.push('animated','fadeInRight')
        }
        let Html = <div></div>
        if(this.state.dataConfig){
            let his = this.state.dataConfig.top

            Html = <div className="content">
                <div className="heading">
                    <h3>{this.state.titlePage}</h3>
                    <h4>{his.title}</h4>
                </div>
                <p>{his.description}</p>
            </div>
        }
        return(
            <div id="his_box" className={h_ani.join(' ')}>
                <div className="inner row-box">
                    <div className="thumbl">
                        <img src="/pictures/his_thumb.jpg" />
                    </div>
                    {Html}
                </div>
            </div>
        )
    }
    _misView(){
        let m_ani = ['a']
        if(this.state.anim){
            m_ani.push('animated','fadeInLeft')
        }
        let Html = <div></div>
        if(this.state.dataConfig){
            let his = this.state.dataConfig.mid
            Html = <div className="content">
                <div className="heading">
                    <h3>{his.title}</h3>
                </div>
                <p>{his.description}</p>
            </div>
        }
        return(
            <div id="mis_box" className={m_ani.join(' ')}>
                <div className="inner row-box margin-30b">
                    {Html}
                    <div className="thumbl">
                        <img src="/pictures/mis_thumb.jpg" />
                    </div>
                </div>
            </div>
        )
    }
    _valView(){
        let Html = <div></div>
        let imgThumb = <img src="/pictures/values.png" />
        if(this.state.dataConfig){
            let his = this.state.dataConfig.value
            Html = <h3>{his.title}</h3>
            imgThumb = <img src={his.thumbnail} />
        }
        return(
            <div id="val_box">
                <div className="inner">
                    <div className="container">
                        <div className="heading">
                            {Html}
                        </div>
                    </div>
                    <div className="thumb">{imgThumb}</div>
                </div>
            </div>
        )
    }
    _humView(){
        let settings = {
              infinite: false,
              speed: 500,
              slidesToShow: 3,
              slidesToScroll: 1,
              initialSlide: 0
        }
        let Html = <div></div>
        if(this.state.dataConfig){
            let his = this.state.dataConfig.human
            Html = <div className="shadow">
                <h2>{his.title}</h2>
                <p>{his.description}</p>
            </div>
        }
        return(
            <div id="hum_box">
                <div className="inner row-box margin-40b">
                    <div className="ct-left relative">
                        <img src="/pictures/thumbleft2.png" />
                        {Html}
                    </div>
                    <div className="ct-right relative">
                        <div className="carousel">
                            <Slider {...settings}>
                                <div className="slide-item">
                                    <a href="/" title="item-1">
                                        <img src="/pictures/ic1.jpg" alt="" />
                                    </a>
                                </div>
                                <div className="slide-item">
                                    <a href="/" title="item-1">
                                        <img src="/pictures/ic2.jpg" alt="" />
                                    </a>
                                </div>
                                <div className="slide-item">
                                    <a href="/" title="item-1">
                                        <img src="/pictures/ic1.jpg" alt="" />
                                    </a>
                                </div>
                                <div className="slide-item">
                                    <a href="/" title="item-1">
                                        <img src="/pictures/ic2.jpg" alt="" />
                                    </a>
                                </div>
                            </Slider>
                        </div>
                    </div>
                </div>
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
            </div>
        )
    }
    render(){
        let homep = this.state.dataMenu ? this.state.dataMenu.home : ''
        let brekum=[
            {
                title: homep,
                link : '/'
            },
            {
                title: this.state.titlePage,
                link : ''
            }
        ]
        let partner = <div></div>
        if(this.state.dataConfig){
            let cn = this.state.dataConfig.customer
            partner = <Partner title={cn.title} description={cn.description} />
        }
        return(
            <section>
                <Mid name={this.state.titlePage} brekum={brekum}/>
                {this._hisView()}
                {this._misView()}
                {this._valView()}
                {this._humView()}
                {partner}
            </section>

        )
    }
}

export default Index
