<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-4 control-label">{{trans('label.user.name')}}</label>
    <div class="col-md-6">
        {!! Form::text('name',old('name'),['class'=>'form-control','id'=>'name','required','placeholder'=>trans('label.user.name')]) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
    <label for="username" class="col-md-4 control-label">{{trans('label.user.username')}}</label>

    <div class="col-md-6">
        {!! Form::text('username',old('username'),['class'=>'form-control','id'=>'username','required','placeholder'=>trans('label.user.username')]) !!}
        @if ($errors->has('username'))
            <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="phone" class="col-md-4 control-label">{{trans('label.user.phone')}}</label>
    <div class="col-md-6">
        {!! Form::text('phone',old('phone'),['class'=>'form-control','id'=>'phone','placeholder'=>trans('label.user.phone')]) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">{{trans('label.user.email')}}</label>

    <div class="col-md-6">
        {!! Form::email('email',old('email'),['class'=>'form-control','required','id'=>'email','placeholder'=>trans('label.user.email')]) !!}
        @if ($errors->has('email'))
            <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="address" class="col-md-4 control-label">{{trans('label.user.address')}}</label>
    <div class="col-md-6">
        {!! Form::text('address',old('phone'),['class'=>'form-control','id'=>'address','placeholder'=>trans('label.user.address')]) !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password" class="col-md-4 control-label">{{trans('label.user.pass')}}</label>

    <div class="col-md-6">
        {!! Form::password('password',['class'=>'form-control','id'=>'password','placeholder'=>trans('label.user.pass')]) !!}
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label for="password-confirm" class="col-md-4 control-label">{{trans('label.user.passcf')}}</label>
    <div class="col-md-6">
        {!! Form::password('password_confirmation',['class'=>'form-control','id'=>'password-confirm','placeholder'=>trans('label.user.pass')]) !!}
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
    <label for="role_id" class="col-md-4 control-label">{{trans('label.user.role')}}</label>
    <div class="col-md-6">
        {!! Form::select('role',$a_role,old('role'),['class'=>'chosen-select form-control','id'=>'role_id']) !!}
        @if ($errors->has('role'))
            <span class="help-block">
                <strong>{{ $errors->first('role') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="u_active" class="col-md-4 control-label no-padding-right">{{trans('label.user.active')}}</label>
    <div class="col-md-6">
        <div class="gr-act">
            {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'u_active']) !!}
            <span class="lbl"></span>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            {{$submit}}
        </button>
    </div>
</div>
