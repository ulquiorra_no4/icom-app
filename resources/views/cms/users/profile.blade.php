@extends('layouts.admin')

@section('breadcrumb')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/">Admin</a>
            </li>
            <li><a href="{{route('user')}}">Users</a></li>
            <li class="active">Profile</li>
        </ul><!-- /.breadcrumb -->
    </div>
@stop
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="clearfix" id="alert_mess">
            @if(Session::has( 'success' ))
            <div class="pull-left alert alert-success no-margin alert-dismissable">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				{{Session::get('success')}}
			</div>
            @endif
		</div>
        <div class="hr dotted"></div>
        <div id="user-profile-1" class="user-profile row">
			<div class="col-xs-12 col-sm-3 center">
				<div>
					<div class="profile-picture" id="thumb_avatar">
						<img id="avatar" class="editable img-responsive" alt="{{$user->name}}" src="{{$user->avatar ? genOriginPic($user->avatar) : '/pictures/user.png'}}" />
                        <div class="box-edit-avatar">
                            <form method="POST" enctype="multipart/form-data">
								<button><span class="fa fa-check-square"></span></button>
                                <label for="m_avatar" id="avt-label"><span class="fa fa-camera"></span></label>
                                <input type="file" name="avatar" id="m_avatar">
                            </form>
                        </div>
					</div>
					<div class="space-4"></div>
					<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
						<div class="inline position-relative">
							<a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
								<i class="ace-icon fa fa-circle light-green"></i>
								&nbsp;
								<span class="white">{{$user->name}}</span>
							</a>
						</div>
					</div>
				</div>
				<div class="space-6"></div>
			</div>
			<div class="col-xs-12 col-sm-9">
                <div class="tabbable">
                    <ul class="nav nav-tabs padding-16">
						<li class="{{ ($errors->has('old_password') || $errors->has('password')) ? '' : 'active' }}">
							<a data-toggle="tab" href="#info" aria-expanded="false">
								Infomation
							</a>
						</li>
						<li class="{{ ($errors->has('old_password') || $errors->has('password')) ? 'active' : '' }}">
							<a data-toggle="tab" href="#epassword" aria-expanded="true">
								<i class="blue ace-icon fa fa-key bigger-125"></i>
								Password
							</a>
						</li>
					</ul>
                    <div class="tab-content profile-edit-tab-content">
                        <div id="info" class="tab-pane {{ ($errors->has('old_password') || $errors->has('password')) ? '' : 'in active' }}">
                            <div class="center">
            					<span class="btn btn-app btn-sm btn-light no-hover">
            						<span class="line-height-1 bigger-170 blue"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Views </span>
            					</span>
            					<span class="btn btn-app btn-sm btn-yellow no-hover">
            						<span class="line-height-1 bigger-170"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Followers </span>
            					</span>
            					<span class="btn btn-app btn-sm btn-pink no-hover">
            						<span class="line-height-1 bigger-170"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Projects </span>
            					</span>
            					<span class="btn btn-app btn-sm btn-grey no-hover">
            						<span class="line-height-1 bigger-170"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Reviews </span>
            					</span>
            					<span class="btn btn-app btn-sm btn-success no-hover">
            						<span class="line-height-1 bigger-170"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Albums </span>
            					</span>
            					<span class="btn btn-app btn-sm btn-primary no-hover">
            						<span class="line-height-1 bigger-170"> 999 </span>
            						<br />
            						<span class="line-height-1 smaller-90"> Contacts </span>
            					</span>
            				</div>
            				<div class="space-12"></div>
            				<div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
            						<div class="profile-info-name">{{trans('label.user.name')}}</div>
            						<div class="profile-info-value">
            							<span class="editable" id="name">{{$user->name}}</span>
            						</div>
            					</div>
            					<div class="profile-info-row">
            						<div class="profile-info-name">{{trans('label.user.username')}}</div>
            						<div class="profile-info-value">
            							<span class="editable" id="username">{{$user->username}}</span>
            						</div>
            					</div>
                                <div class="profile-info-row">
            						<div class="profile-info-name">{{trans('label.user.email')}}</div>
            						<div class="profile-info-value">
                                        <i class="fa fa-envelope light-orange bigger-110"></i>
            							<span class="editable" id="email">{{$user->email}}</span>
            						</div>
            					</div>
            					<div class="profile-info-row">
            						<div class="profile-info-name">{{trans('label.user.address')}}</div>
            						<div class="profile-info-value">
            							<i class="fa fa-map-marker light-orange bigger-110"></i>
            							<span class="editable" id="address">{{$user->address}}</span>
            						</div>
            					</div>
            					<div class="profile-info-row">
            						<div class="profile-info-name">{{trans('label.created_at')}}</div>
            						<div class="profile-info-value">
            							<span class="editable">{{$user->created_at}}</span>
            						</div>
            					</div>
            				</div>
                        </div>
                        <div id="epassword" class="tab-pane {{ ($errors->has('old_password') || $errors->has('password')) ? 'in active' : '' }}">
                            <form class="form form-horizontal" action="{{route('user_update_security')}}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
    								<label class="col-sm-3 control-label no-padding-right" for="e_password_cur">Old password</label>
    								<div class="col-sm-4">
    									<input id="e_password_cur" type="password" name="old_password" class="form-control">
    								</div>
                                    @if ($errors->has('old_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('old_password') }}</strong>
                                        </span>
                                    @endif
    							</div>
    							<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
    								<label class="col-sm-3 control-label no-padding-right" for="e_password">New Password</label>
    								<div class="col-sm-4">
    									<input id="e_password" type="password" name="password" class="form-control">
    								</div>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
    							</div>
    							<div class="form-group">
    								<label class="col-sm-3 control-label no-padding-right" for="e_password_conf">Confirm Password</label>
    								<div class="col-sm-4">
    									<input id="e_password_conf" type="password" name="password_confirmation" class="form-control">
    								</div>
    							</div>
                                <div class="clearfix">
									<div class="col-md-offset-3 col-md-4">
										<button class="btn btn-info">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Update
										</button>
										<button class="btn" type="reset">
											<i class="ace-icon fa fa-undo bigger-110"></i>
											Cancel
										</button>
									</div>
								</div>
                            </form>
                        </div>
                    </div>
                </div>
				<div class="space-20"></div>
			</div>
		</div>
    </div>
</div>
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@stop
@section('style')
@stop
@section('script')
<script src="{{asset('js/admin/file-upload.min.js')}}"></script>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function(){
    $('#m_avatar').change(function () {
        $('#thumb_avatar img').css('visibility','hidden');
        $('.box-edit-avatar button').css('visibility','visible');
    });
    $.uploadPreview({
        input_field: "#m_avatar",   // Default: .image-upload
        preview_box: "#thumb_avatar",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false                 // Default: false
    });
    $('.profile-picture form').on('submit',function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        // console.log(formData);
        $.ajax({
            url: "{{route('user_update_avatar')}}",
            type: "POST",
            data:  formData,
            beforeSend: function(){},
            contentType: false,
            processData:false,
            success: function(resp)
            {
                // console.log(resp);
                $('#thumb_avatar img').removeAttr('style');
                if(!resp.error){
                    $('#thumb_avatar img').remove();
                    $('#thumb_avatar').append('<img src="'+ resp.source +'">');
                }
                $('#alert_mess').append('<div class="pull-left alert alert-success no-margin alert-dismissable"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>'+resp.mess+'</div>');
            },
            error: function()
            {
            }
        });
    });
})
</script>
@stop
