@extends('layouts.admin')

@section('breadcrumb')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/admin">Admin</a>
            </li>
            <li class="active"><a href="{{route('user')}}">Users</a></li>
        </ul><!-- /.breadcrumb -->
    </div>
@stop
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="header smaller lighter blue">{{$head_title}}</h3>
                    <div class="clearfix">
                        <div class="pull-right tableTools-container"></div>
                    </div>
                    <div class="table-header">
                        Results for Users
                    </div>
                    <div>
                        <table id="main_table" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="center">
                                    <label class="pos-rel">
                                        <input type="checkbox" class="ace"/>
                                        <span class="lbl"></span>
                                    </label>
                                </th>
                                @foreach($table_head as $a_field)
                                    <th class="sorting" tabindex="0" aria-controls="main_table" rowspan="1" colspan="1"
                                        aria-label="{{$a_field['name']}}"
                                        aria-sort="ascending">{{$a_field['name']}}</th>
                                @endforeach
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr id="{{$user->id}}">
                                    <td class="center">
                                        <label class="pos-rel">
                                            <input class="ace" type="checkbox" value="{{$user->id}}"/>
                                            <span class="lbl"></span>
                                        </label>
                                    </td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td class="hidden-480 sorting_1">
                                        <span class="label label-sm label-info arrowed arrowed-righ">{{$user->role}}</span>
                                    </td>
                                    <td class="hidden-480 sorting_1">
                                        <span class="label label-sm label-warning">{{$user->active?'Active':'Block'}}</span>
                                    </td>
                                    <td>{{$user->created_at}}</td>
                                    <td>
                                        <div class="hidden-sm hidden-xs action-buttons">
                                            <a class="green" href="{{route('user_edit',['id'=>$user->id])}}"><i
                                                        class="ace-icon fa fa-pencil bigger-130"></i></a>
                                            <a class="red" href="{{route('user_destroy',['id'=>$user->id])}}"
                                               onclick="event.preventDefault();aj_delete_one(this)"><i
                                                        class="ace-icon fa fa-trash-o bigger-130"></i>
                                            </a>
                                        </div>
                                        <div class="hidden-md hidden-lg">
                                            <div class="inline pos-rel">
                                                <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                        data-toggle="dropdown" data-position="auto">
                                                    <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                    <li>
                                                        <a href="#" class="tooltip-info" data-rel="tooltip" title=""
                                                           data-original-title="View">
                                                    <span class="blue">
                                                        <i class="ace-icon fa fa-search-plus bigger-120"></i>
                                                    </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="tooltip-success" data-rel="tooltip" title=""
                                                           data-original-title="Edit">
                                                    <span class="green">
                                                        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                    </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="tooltip-error" data-rel="tooltip" title=""
                                                           data-original-title="Delete">
                                                    <span class="red">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div>
    <div id="dialog_box"></div>
@stop

@section('script')
    <script src="{{asset('js/admin/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/admin/dataTables.select.min.js')}}"></script>
    <script src="{{asset('js/admin/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('js/admin/jquery-ui.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function aj_delete_one(obj) {
            var url = obj.href;
            $('#dialog_box').dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                title: "{{trans('alert.title.delete')}}",
                buttons: {
                    "Yes": function () {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            success: function (resp) {
                                $( "#dialog_box" ).dialog({
                                    modal: true,
                                    title: resp.mess,
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                });
                                if(!resp.error){
                                    window.location.reload(true);
                                }
                            }
                        });
                        $(this).dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });

        }

        function aj_multi_delete() {
            var arrId = [];
            var retu = false;
            $('#main_table tbody').find('input:checked').each(function () {
                arrId.push($(this).val());
            });

            $('#dialog_box').dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                title: "{{trans('alert.title.multiDel')}}",
                buttons: {
                    "Yes": function () {
                        $.ajax({
                            url: '{{route('user_multi_destroy')}}',
                            type: 'POST',
                            dataType: 'json',
                            data: {id: arrId},
                            success: function (resp) {
                                $( "#dialog_box" ).dialog({
                                    modal: true,
                                    title: resp.mess,
                                    buttons: {
                                        Ok: function() {
                                            $(this).dialog( "close" );
                                        }
                                    }
                                });
                            }
                        });

                        // $(this).dialog("close");
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });
            return true;
        }
        jQuery(function ($) {
            var myTable =
                $('#main_table')
                    .DataTable({
                        ordering: true,
                        searching: true,
                        stateSave: true,
                        "columns": [
                            {"orderable": false},
                            null,
                            null,
                            null,
                            null,
                            null,
                            {"orderable": false}
                        ],
                        select: {
                            style: 'multi'
                        }
                    });
            $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
            new $.fn.dataTable.Buttons(myTable, {
                buttons: [
                    {
                        "action": function (e, dt, button, config) {
                            return aj_multi_delete() && this.rows({selected:  true}).remove().draw();
                        },
                        "text": "<i class='fa fa-trash bigger-110 red'></i> <span class='hidden'>Delete item selected</span>",
                        "className": "btn btn-white btn-primary btn-bold"
                    }

                ]
            });
            myTable.buttons().container().appendTo($('.tableTools-container'));
            myTable.on('select', function (e, dt, type, index) {
                if (type === 'row') {
                    $(myTable.row(index).node()).find('input:checkbox').prop('checked', true);
                }
            });
            myTable.on('deselect', function (e, dt, type, index) {
                if (type === 'row') {
                    $(myTable.row(index).node()).find('input:checkbox').prop('checked', false);
                }
            });

            $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
            $('#main_table > thead > tr > th input[type=checkbox], #main_table_wrapper input[type=checkbox]').eq(0).on('click', function () {
                var th_checked = this.checked;//checkbox inside "TH" table header
                $('#main_table').find('tbody > tr').each(function () {
                    var row = this;
                    if (th_checked) myTable.row(row).select();

                    else myTable.row(row).deselect();
                });
            });
        })
    </script>
@stop
