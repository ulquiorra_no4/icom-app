<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_name">{{trans('label.menu.name')}} <span class="red-bold">(*)</span></label>
            <div class="col-sm-9 {{$errors->has('m_name')?'error':''}}">
                {!! Form::text('menu_name',old('menu_name'),['class'=>'col-xs-10 col-sm-12','id'=>'m_name','required','placeholder'=>trans('label.menu.name')]) !!}
                @if ($errors->has('menu_name'))
                    <p class="help-block col-sm-12">{{$errors->first('menu_name')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_name_en">{{trans('label.menu.nameEn')}}</label>
            <div class="col-sm-9 {{$errors->has('menu_name_en')?'error':''}}">
                {!! Form::text('menu_name_en',old('menu_name_en'),['class'=>'col-xs-10 col-sm-12','id'=>'m_name_en','placeholder'=>trans('label.menu.nameEn')]) !!}
                @if ($errors->has('menu_name_en'))
                    <p class="help-block col-sm-12">{{$errors->first('menu_name_en')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_alias">{{trans('label.menu.alias')}}</label>
            <div class="col-sm-9 {{$errors->has('menu_alias')?'error':''}}">
                {!! Form::text('menu_alias',old('menu_alias'),['class'=>'col-xs-10 col-sm-12','id'=>'m_alias','placeholder'=>trans('label.menu.alias')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_link">{{trans('label.menu.link')}}</label>
            <div class="col-sm-9 {{$errors->has('menu_link')?'error':''}}">
                {!! Form::text('menu_link',old('menu_link'),['class'=>'col-xs-10 col-sm-12','id'=>'m_link','placeholder'=>trans('label.menu.link')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_order">{{trans('label.menu.order')}}</label>
            <div class="col-sm-9 {{$errors->has('order')?'error':''}}">
                {!! Form::text('order',old('order'),['class'=>'col-xs-10 col-sm-12','id'=>'m_order','placeholder'=>trans('label.menu.order')]) !!}
                @if ($errors->has('order'))
                    <p class="help-block col-sm-12">{{$errors->first('order')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="m_icon">{{trans('label.menu.icon')}}</label>
            <div class="col-sm-9 {{$errors->has('menu_icon')?'error':''}}">
                {!! Form::text('menu_icon',old('menu_icon'),['class'=>'col-xs-10 col-sm-12','id'=>'m_icon','placeholder'=>trans('label.menu.icon')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="m_active" class="col-sm-3 control-label no-padding-right">{{trans('label.menu.active')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'m_active','checked']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
