<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="mod_code">{{trans('label.module.code')}} <span class="red-bold">(*)</span></label>
    <div class="col-sm-10 {{$errors->has('mod_code')?'error':''}}">
        {!! Form::text('mod_code',old('mod_code'),['class'=>'col-xs-10 col-sm-5','id'=>'mod_code','required','placeholder'=>trans('label.module.code')]) !!}
        @if ($errors->has('mod_code'))
            <p class="help-block col-sm-12">{{$errors->first('mod_code')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="mod_name">{{trans('label.module.name')}} <span class="red-bold">(*)</span></label>
    <div class="col-sm-10 {{$errors->has('mod_name')?'error':''}}">
        {!! Form::text('mod_name',old('mod_name'),['class'=>'col-xs-10 col-sm-5','id'=>'mod_name','required','placeholder'=>trans('label.module.name')]) !!}
        @if ($errors->has('mod_name'))
            <p class="help-block col-sm-12">{{$errors->first('mod_name')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="mod_name_en">{{trans('label.module.nameEn')}}</label>
    <div class="col-sm-10 {{$errors->has('mod_name_en')?'error':''}}">
        {!! Form::text('mod_name_en',old('mod_name_en'),['class'=>'col-xs-10 col-sm-5','id'=>'mod_name_en','placeholder'=>trans('label.module.nameEn')]) !!}
        @if ($errors->has('mod_name_en'))
            <p class="help-block col-sm-12">{{$errors->first('mod_name_en')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="mod_alias">{{trans('label.module.alias')}}</label>
    <div class="col-sm-10 {{$errors->has('mod_alias')?'error':''}}">
        {!! Form::text('mod_alias',old('mod_alias'),['class'=>'col-xs-10 col-sm-5','id'=>'mod_alias','placeholder'=>trans('label.module.alias')]) !!}
        @if ($errors->has('mod_alias'))
            <p class="help-block col-sm-12">{{$errors->first('mod_alias')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="mod_desc" class="col-sm-2 control-label no-padding-right">{{trans('label.module.description')}}</label>
    <div class="col-sm-10 {{$errors->has('mod_description')?'error':''}}">
        {!! Form::textarea('mod_description',old('mod_description'),['class'=>'col-xs-10 col-sm-5','rows'=>4,'id'=>'mod_desc']) !!}
        @if ($errors->has('mod_description'))
            <p class="help-block col-sm-12">{{$errors->first('mod_description')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label no-padding-right" for="mod_order">{{trans('label.module.order')}}</label>
    <div class="col-sm-10 {{$errors->has('order')?'error':''}}">
        {!! Form::number('order',old('order'),['class'=>'col-xs-10 col-sm-5','id'=>'mod_order','placeholder'=>trans('label.module.order')]) !!}
        @if ($errors->has('order'))
            <p class="help-block col-sm-12">{{$errors->first('order')}}</p>
        @endif
    </div>
</div>
<div class="form-group">
    <label for="mod_active" class="col-sm-2 control-label no-padding-right">{{trans('label.module.active')}}</label>
    <div class="col-sm-10">
        <div class="gr-act">
            {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'mod_active']) !!}
            <span class="lbl"></span>
        </div>
    </div>
</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>