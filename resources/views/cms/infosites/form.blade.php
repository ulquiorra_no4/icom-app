<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="iw_title">{{trans('label.infosite.title')}} <span class="red-bold">(*)</span></label>
            <div class="col-sm-9 {{$errors->has('inf_title')?'error':''}}">
                {!! Form::text('inf_title',old('inf_title'),['class'=>'col-xs-10 col-sm-12','id'=>'iw_title','required','placeholder'=>trans('label.infosite.title')]) !!}
                @if ($errors->has('inf_title'))
                    <p class="help-block col-sm-12">{{$errors->first('inf_title')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="iw_title_en">{{trans('label.infosite.titleEn')}}</label>
            <div class="col-sm-9 {{$errors->has('inf_title_en')?'error':''}}">
                {!! Form::text('inf_title_en',old('inf_title_en'),['class'=>'col-xs-10 col-sm-12','id'=>'iw_title_en','placeholder'=>trans('label.infosite.titleEn')]) !!}
                @if ($errors->has('inf_title_en'))
                    <p class="help-block col-sm-12">{{$errors->first('inf_title_en')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="iw_desc">{{trans('label.infosite.description')}}</label>
            <div class="col-sm-9 {{$errors->has('inf_description')?'error':''}}">
                {!! Form::textarea('inf_description',old('inf_description'),['class'=>'col-xs-10 col-sm-12','rows'=>3,'id'=>'iw_desc']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="iw_desc_en">{{trans('label.infosite.descriptionEn')}}</label>
            <div class="col-sm-9 {{$errors->has('inf_description_en')?'error':''}}">
                {!! Form::textarea('inf_description_en',old('inf_description_en'),['class'=>'col-xs-10 col-sm-12','rows'=>3,'id'=>'iw_desc_en']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="iw_order">{{trans('label.infosite.order')}}</label>
            <div class="col-sm-9 {{$errors->has('order')?'error':''}}">
                {!! Form::text('order',old('order'),['class'=>'col-xs-10 col-sm-12','id'=>'iw_order','placeholder'=>trans('label.infosite.order')]) !!}
                @if ($errors->has('order'))
                    <p class="help-block col-sm-12">{{$errors->first('order')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="iw_active" class="col-sm-3 control-label no-padding-right">{{trans('label.infosite.active')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'iw_active','checked']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="page_id" class="col-sm-3 control-label no-padding-right">{{trans('label.infosite.page')}}</label>
            <div class="col-sm-9">
                {!! Form::select('inf_page_id',$menus,old('inf_page_id'),['class'=>'col-xs-10 col-sm-12 chosen-select form-control','id'=>'page_id']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="iw_position" class="col-sm-3 control-label no-padding-right">{{trans('label.infosite.position')}}</label>
            <div class="col-sm-9">
                {!! Form::select('inf_position',$positions,old('inf_position'),['class'=>'col-xs-10 col-sm-12 chosen-select form-control','id'=>'iw_position']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="iw_isSub" class="col-sm-3 control-label no-padding-right">{{trans('label.infosite.sub')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('inf_isSub',1,old('inf_isSub'),['class'=>'ace ace-switch ace-switch-3','id'=>'iw_isSub']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
@section('script')
<script src="{{asset('plugins/summernote/summernote-bs4.js')}}"></script>
<script type="text/javascript">
    jQuery(function($) {
        $('#iw_desc').summernote();
        $('#iw_desc_en').summernote();
    });
</script>
@stop
