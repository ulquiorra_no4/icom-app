@extends('layouts.admin')
@section('breadcrumb')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/admin">Admin</a>
            </li>
            <li><a href="{{route('pos')}}">Posts</a></li>
            <li class="active">Edit</li>
        </ul><!-- /.breadcrumb -->
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if(Session::has('alert_success'))
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    {{Session::get('alert_success')}}
                </div>
            @endif
            <h4 class="lighter block green lighter-heading"><span class="red-bold">(*)</span> {{trans('label.note.required')}}</h4>
            {!! Form::model($post,['action'=>['Cms\PostController@update',$post->id],'class'=>'form','role'=>'form','method'=>'PATCH','enctype' => 'multipart/form-data']) !!}
            @include('cms.posts.form',['submit'=>trans('label.update')])
            {!! Form::close() !!}
        </div>
    </div>
@stop
