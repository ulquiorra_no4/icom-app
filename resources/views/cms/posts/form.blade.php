<div class="row">
    <div class="col-md-8">
        <div class="form-group {{$errors->has('title')?'has-error':''}}">
            <label for="pos_title">{{trans('label.post.title')}} <span class="red-bold">(*)</span></label>
            {!! Form::text('title',old('title'),['class'=>'col-xs-12 form-control','id'=>'pos_title','required','placeholder'=>trans('label.post.title')]) !!}
            @if ($errors->has('title'))
                <p class="help-block col-sm-12">{{$errors->first('title')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_title_en">{{trans('label.post.titleEn')}}</label>
            {!! Form::text('title_en',old('title_en'),['class'=>'col-xs-12 form-control','id'=>'pos_title_en','placeholder'=>trans('label.post.titleEn')]) !!}
            @if ($errors->has('title_en'))
                <p class="help-block col-sm-12">{{$errors->first('title_en')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_desc">{{trans('label.post.description')}}</label>
            {!! Form::textarea('description',old('description'),['class'=>'col-xs-12 form-control','id'=>'pos_desc','rows'=>4]) !!}
        </div>
        <div class="form-group">
            <label for="pos_desc_en">{{trans('label.post.descriptionEn')}}</label>
            {!! Form::textarea('description_en',old('description_en'),['class'=>'col-xs-12 form-control','id'=>'pos_desc','rows'=>4]) !!}
        </div>
        <div class="form-group">
            <label for="pos_keyword">{{trans('label.post.keywords')}}</label>
            <div class="tag-group">
                {!! Form::text('keywords',old('keywords'),['class'=>'col-xs-12 form-control','id'=>'pos_keyword','placeholder'=>trans('label.post.keywords')]) !!}
            </div>
        </div>
        <div class="form-group {{$errors->has('content')?'has-error':''}}">
            <label for="pos_content">{{trans('label.post.content')}} <span class="red-bold">(*)</span></label>
            {!! Form::textarea('content',old('content'),['class'=>'col-xs-12 form-control','id'=>'pos_content','rows'=>10,'required']) !!}
            @if ($errors->has('content'))
                <p class="help-block col-sm-12">{{$errors->first('content')}}</p>
            @endif
        </div>
        <div class="form-group {{$errors->has('content_en')?'has-error':''}}">
            <label for="pos_content_en">{{trans('label.post.contentEn')}}</label>
            {!! Form::textarea('content_en',old('content_en'),['class'=>'col-xs-12 form-control','id'=>'pos_content_en','rows'=>10]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{$errors->has('avatar')?'has-error':''}}">
            <label for="pos_avatar">{{trans('label.post.avatar')}} <span class="red-bold">(*)</span></label>
            {!! Form::file('avatar',['class'=>'col-xs-12','id'=>'pos_avatar','multiple'=>'','required']) !!}
            {!! Form::hidden('avatar_file',old('avatar'),[]) !!}
            @if ($errors->has('avatar'))
                <p class="help-block col-sm-12">{{$errors->first('avatar')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_cat_id">{{trans('label.post.catId')}}</label>
            {!! Form::select('category_id',$a_categories,old('category_id'),['class'=>'col-xs-12 chosen-select form-control','id'=>'pos_cat_id']) !!}
        </div>
        <div class="form-group">
            <label for="pos_tags">{{trans('label.post.tag')}}</label>
            <div class="tag-group">
                {!! Form::text('tags',old('tags'),['class'=>'col-xs-12 form-control','id'=>'pos_tags','placeholder'=>trans('label.post.tag')]) !!}
            </div>
        </div>
        <div class="form-group {{$errors->has('salary')?'has-error':''}}">
            <label for="pos_salary">{{trans('label.post.salary')}} (Tin tuyển dụng)</label>
            {!! Form::text('salary',old('salary'),['class'=>'col-xs-12 form-control','id'=>'pos_salary','placeholder'=>trans('label.post.salary')]) !!}
            @if ($errors->has('salary'))
                <p class="help-block col-sm-12">{{$errors->first('salary')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_place">{{trans('label.post.place')}} (Tin tuyển dụng)</label>
            {!! Form::text('place',old('place'),['class'=>'col-xs-12 form-control','id'=>'pos_place','placeholder'=>trans('label.post.place')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_position">{{trans('label.post.position')}} (Tin tuyển dụng)</label>
            {!! Form::text('position',old('position'),['class'=>'col-xs-12 form-control','id'=>'pos_position','placeholder'=>trans('label.post.position')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_options">{{trans('label.post.option')}}</label>
            <div class="tag-group">
                {!! Form::text('option',old('option'),['class'=>'col-xs-12 form-control','id'=>'pos_options','placeholder'=>trans('label.post.option')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="pos_end_time">{{trans('label.post.endTime')}} (Tin tuyển dụng)</label>
            {!! Form::text('end_time',old('end_time'),['class'=>'col-xs-12 form-control','id'=>'pos_end_time','placeholder'=>trans('label.post.endTime')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_active_time">{{trans('label.post.activeTime')}}</label>
            {!! Form::text('active_time',old('active_time'),['class'=>'col-xs-12 form-control','id'=>'pos_active_time','placeholder'=>trans('label.post.activeTime')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_active" class="col-sm-3 control-label no-padding-right">{{trans('label.post.active')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'pos_active']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>

    </div>

</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
@section('style')
<link rel="stylesheet" href="{{asset('css/admin/bootstrap-datepicker.min.css')}}">
@stop
@section('script')
<script src="{{asset('plugins/summernote/summernote-bs4.js')}}"></script>
<script src="{{asset('js/admin/bootstrap-tag.min.js')}}"></script>
<script src="{{asset('js/admin/bootstrap-datepicker.min.js')}}"></script>
<script>
    jQuery(function() {
        var a_cat = {!! $a_categories->toJson(); !!};
        var tag_keyword = $('#pos_keyword');
        var tag_input = $('#pos_tags');
        var tag_option = $('#pos_options');
        try{
            tag_keyword.tag({
                placeholder:tag_keyword.attr('placeholder'),
                source: a_cat,
            })
            tag_input.tag({
                placeholder:tag_input.attr('placeholder'),
            })
            tag_option.tag({
                placeholder:tag_option.attr('placeholder'),
            })
            var $tag_obj2 = $('#pos_tags').data('tag');
            var index2 = $tag_obj2.inValues('some tag');
            $tag_obj2.remove(index2);
            var $tag_obj = $('#pos_tags').data('tag');
            var index = $tag_obj.inValues('some tag');
            $tag_obj.remove(index);
            var $tag_obj4 = tag_option.data('tag');
            var index4 = $tag_obj4.inValues('news tag');
            $tag_obj4.remove(index4);
        }catch(e){
            tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
        }

        $('#pos_active_time').datepicker({
            format: 'mm-dd-yyyy'
        });
        $('#pos_end_time').datepicker({
            format: 'mm-dd-yyyy'
        });
        $('#pos_avatar').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small',
            preview_error : function(filename, error_code) {
            }
        }).on('change', function(){
            console.log($(this).data('ace_input_files'));
        });
        $('#pos_content,#pos_content_en').summernote();
    });
</script>
@stop
