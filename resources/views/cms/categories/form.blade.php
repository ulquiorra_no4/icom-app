<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="cat_name">{{trans('label.category.name')}} <span class="red-bold">(*)</span></label>
            <div class="col-sm-9 {{$errors->has('cat_name')?'error':''}}">
                {!! Form::text('cat_name',old('cat_name'),['class'=>'col-xs-10 col-sm-12','id'=>'cat_name','required','placeholder'=>trans('label.category.name')]) !!}
                @if ($errors->has('cat_name'))
                    <p class="help-block col-sm-12">{{$errors->first('cat_name')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="cat_name_en">{{trans('label.category.nameEn')}}</label>
            <div class="col-sm-9 {{$errors->has('cat_name_en')?'error':''}}">
                {!! Form::text('cat_name_en',old('cat_name_en'),['class'=>'col-xs-10 col-sm-12','id'=>'cat_name_en','placeholder'=>trans('label.category.nameEn')]) !!}
                @if ($errors->has('cat_name_en'))
                    <p class="help-block col-sm-12">{{$errors->first('cat_name_en')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="cat_alias">{{trans('label.category.alias')}}</label>
            <div class="col-sm-9 {{$errors->has('cat_alias')?'error':''}}">
                {!! Form::text('cat_alias',old('cat_alias'),['class'=>'col-xs-10 col-sm-12','id'=>'cat_alias','placeholder'=>trans('label.category.alias')]) !!}
                @if ($errors->has('cat_alias'))
                    <p class="help-block col-sm-12">{{$errors->first('cat_alias')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="cat_desc" class="col-sm-3 control-label no-padding-right">{{trans('label.category.description')}}</label>
            <div class="col-sm-9 {{$errors->has('cat_description')?'error':''}}">
                {!! Form::textarea('cat_description',old('cat_description'),['class'=>'col-xs-10 col-sm-12','rows'=>4,'id'=>'cat_desc']) !!}
                @if ($errors->has('cat_description'))
                    <p class="help-block col-sm-12">{{$errors->first('cat_description')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="cat_order">{{trans('label.category.order')}}</label>
            <div class="col-sm-9 {{$errors->has('order')?'error':''}}">
                {!! Form::number('order',old('order'),['class'=>'col-xs-10 col-sm-12','id'=>'cat_order','placeholder'=>trans('label.category.order')]) !!}
                @if ($errors->has('order'))
                    <p class="help-block col-sm-12">{{$errors->first('order')}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="cat_active" class="col-sm-3 control-label no-padding-right">{{trans('label.category.active')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'cat_active']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="cat_parent_id" class="col-sm-3 control-label no-padding-right">{{trans('label.category.catParent')}}</label>
            <div class="col-sm-9">
                {!! Form::select('cat_parent_id',$a_cat_parent,old('cat_parent_id'),['class'=>'col-xs-10 col-sm-12 chosen-select form-control','id'=>'cat_parent_id','data-placeholder'=>'Choose category']) !!}
            </div>
        </div>
    </div>
</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
