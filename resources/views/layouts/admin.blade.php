<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <meta content="IE=edge" http-equiv=X-UA-Compatible>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    @yield('meta')
    <title>I-com Manager</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel=stylesheet>
    <link href="{{asset('css/font-awesome.min.css')}}" rel=stylesheet>
    <link href="{{asset('css/fonts.googleapis.com.css')}}" rel=stylesheet>
    <link href="{{asset('css/admin/ace.min.css')}}" rel=stylesheet>
    <link rel="stylesheet" href="{{asset('css/admin/ace-skins.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/admin/ace-rtl.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/admin/admin.css')}}" />
    <link rel="stylesheet" href="{{asset('css/admin/jquery-ui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/admin/chosen.min.css')}}" />
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}" />
    @yield('style')
</head>
<body class="no-skin">
@include('layouts.ad_navbar')
<div class="main-container ace-save-state" id="main-container">
    @include('layouts.ad_sidebar')
    <div class="main-content">
        <div class="main-content-inner">
            @yield('breadcrumb')
            @include('layouts.ad_setting')
            <div class="page-content content2">
                @yield('content')
            </div>
        </div>
    </div>
    @include('layouts.ad_footer')
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<!-- SCRIPT -->
<script src="{{asset('js/admin/ace-extra.min.js')}}"></script>
<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('js/admin/ace-elements.min.js')}}"></script>
<script src="{{asset('js/admin/moment.min.js')}}"></script>
<script src="{{asset('js/admin/ace.min.js')}}"></script>
<script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/chosen.jquery.min.js')}}"></script>
@yield('script')
<!-- END_SCRIPT -->

</body>
</html>
