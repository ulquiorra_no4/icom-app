<header id="header">
	<div class="container">
		<nav class="navbar">
			<h1 class="top-logo">
				<a href="/" title="I-com.vn"><img src="/images/logo_1.png" alt="I-com.vn"></a>
			</h1>
			<div class="navbar-menu">
				<button class="btn" id="toggle_menu"><span class="fa fa-bars"></span></button>
				<ul class="top-menu">
					@foreach(array_values($menu) as $k => $child)
					<li class="{{$m_act==$k?'active':''}}"><a href="{{$child['link']}}" title="{{$child['title']}}">{{$child['title']}}</a></li>
					@endforeach
				</ul>
			</div>
			<ul class="lang-opt">
				<li><a href="javascript:set_lang(0);" class="vn">VN</a></li>
				<li><a href="javascript:set_lang(1);" class="en">EN</a></li>
			</ul>
		</nav>

	</div>
</header>
