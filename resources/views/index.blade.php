<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Công ty CP Dịch vụ Truyền thông VietNamNet Icom</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/stylesheet.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/fontawesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('css/animate.min.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="main"></div>
        <script src="{{asset('js/app.js')}}" ></script>
    </body>
</html>
