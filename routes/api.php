<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/get_config/{name}',['uses'=>'ConfigController@getConfigApp']);
Route::post('/set_lang',['uses'=>'LanguageController@setLang']);
Route::get('/get_lang',['uses'=>'LanguageController@getCurentLocale']);
Route::get('/post',['uses'=>'PostController@getAll']);
Route::get('/post_detail/{id}',['uses'=>'PostController@getById']);
Route::get('/get_conf_menu',['uses'=>'ConfigController@getMenu2']);
Route::get('/get_services',['uses'=>'ServiceController@getServices']);
Route::get('/ser_detail/{params}',['uses'=>'ServiceController@getServiceDetail']);
Route::get('/news_page_info',['uses'=>'PostController@getInfo']);
Route::get('/get_news/{page}',['uses'=>'PostController@getNews']);
Route::get('/news_detail/{params}',['uses'=>'PostController@getNewsDetail']);
Route::get('/get_side_news',['uses'=>'PostController@getSideMenuById']);
Route::get('/requirement_page_info',['uses'=>'RequirementController@getInfo']);
Route::get('/get_requirement/{page}',['uses'=>'RequirementController@getRequirement']);
Route::get('/requirement_detail/{params}',['uses'=>'RequirementController@getRequirementDetail']);
Route::get('/search_news/{keyword}/{page}',['uses'=>'PostController@search']);
Route::get('/search_requirement/{keyword}',['uses'=>'RequirementController@search']);
// Route::get('/get_menu_config',['uses'=>'ConfigController@getMenu']);
