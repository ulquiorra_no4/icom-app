<?php

namespace App\Models\Menus;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    //
    protected $table = 'menuses';
    protected $fillable = ['menu_name','menu_name_en','menu_alias','menu_link','active','menu_icon','order'];
    protected $primaryKey = 'menu_id';

    /**
     * Scope a query to only include active category.
     * @param $query
     * @return mixed
     */
    public function scopeActive($query){
        return $query->where('active', 1);
    }

    public function scopeSort($query){
        return $query->orderBy('order', 'ASC');
    }
}
