<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'cat_id';
    protected $fillable = ['cat_name', 'cat_name_en', 'cat_alias', 'cat_description', 'cat_parent_id', 'cat_type', 'cat_has_child', 'active', 'order'];

    public function posts(){
        return $this->hasMany('App\Models\Posts','category_id',$this->primaryKey);
    }

    /**
     * Scope a query to only include active category.
     * @param $query
     * @return mixed
     */
    public function scopeActive($query){
        return $query->where('active', 1);
    }
}
