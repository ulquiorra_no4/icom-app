<?php

namespace App\Policies;

use App\Models\Users\Admin;
use App\Models\InfoSites\InfoSites;
use Illuminate\Auth\Access\HandlesAuthorization;

class InfoSitesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can do something the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function right(Admin $user){
        return in_array($user->role,['administrator','admin']);
    }
    /**
     * Determine whether the user can view the infoSites.
     *
     * @param  \App\Models\Users\Admin  $user
     * @param  \App\InfoSites  $infoSites
     * @return mixed
     */
    public function view(Admin $user, InfoSites $infoSites)
    {
        //
    }

    /**
     * Determine whether the user can create infoSites.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can update the infoSites.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function update(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can delete the infoSites.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function delete(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }
}
