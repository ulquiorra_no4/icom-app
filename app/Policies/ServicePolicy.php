<?php

namespace App\Policies;

use App\Models\Users\Admin;
use App\Models\Services\Services;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can do something the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function right(Admin $user){
        return in_array($user->role,['administrator','admin']);
    }
    /**
     * Determine whether the user can view the services.
     *
     * @param  \App\Models\Users\Admin  $user
     * @param  \App\Models\Services\Services  $services
     * @return mixed
     */
    public function view(Admin $user, Services $services)
    {
        //
    }

    /**
     * Determine whether the user can create services.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return in_array($user->role,['administrator','admin','poster']);
    }

    /**
     * Determine whether the user can update the services.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function update(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can delete the services.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function delete(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }
}
