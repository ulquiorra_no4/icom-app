<?php

namespace App\Policies;

use App\Models\Users\Admin;
use App\Models\Menus\Menus;
use Illuminate\Auth\Access\HandlesAuthorization;

class MenusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can do something the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function right(Admin $user){
        return in_array($user->role,['administrator','admin']);
    }
    /**
     * Determine whether the user can view the menus.
     *
     * @param  \App\Models\Users\Admin  $user
     * @param  \App\Models\Menus\Menus  $menus
     * @return mixed
     */
    public function view(Admin $user, Menus $menus)
    {

    }

    /**
     * Determine whether the user can create menuses.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can update the menus.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function update(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can delete the menus.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function delete(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }
}
