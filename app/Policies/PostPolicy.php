<?php

namespace App\Policies;

use App\Models\Users\Admin;
use App\Models\Posts\Posts;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can do something the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function right(Admin $user){
        return in_array($user->role,['administrator','admin']);
    }
    /**
     * Determine whether the user can view the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @param  \App\Models\Posts\Posts  $posts
     * @return mixed
     */
    public function view(Admin $user, Posts $posts)
    {
        //
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return in_array($user->role,['administrator','admin','poster']);
    }

    /**
     * Determine whether the user can update the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @param  \App\Models\Posts\Posts  $posts
     * @return mixed
     */
    public function update(Admin $user, Posts $posts)
    {
        return $user->role==='administrator'||$user->role==='admin'||$user->id===$posts->author_id;
    }

    /**
     * Determine whether the user can delete the posts.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function delete(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }
}
