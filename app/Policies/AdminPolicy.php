<?php

namespace App\Policies;

use App\Models\Users\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;


    public function right(Admin $user)
    {
        return $user->role==='administrator';
    }
    /**
     * Determine whether the user can view the admin.
     *
     * @param  \App\Models\Users\Admin  $admin
     * @return mixed
     */
    public function view(Admin $user)
    {
        return in_array($user->role,['administrator','admin']);
    }

    /**
     * Determine whether the user can create admins.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        return $user->role==='administrator';
    }

    /**
     * Determine whether the user can update the admin.
     *
     * @param  \App\Models\Users\Admin  $admin
     * @return mixed
     */
    public function update(Admin $user, Admin $admin)
    {
        return $user->role==='administrator' || $user->id===$admin->id;
    }

    /**
     * Determine whether the user can delete the admin.
     *
     * @param  \App\Models\Users\Admin  $admin
     * @return mixed
     */
    public function delete(Admin $user)
    {
        return $user->role==='administrator';
    }
}
