<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/16/2017
 * Time: 2:35 PM
 */
function generatePathFile($path_file, $file_name)
{
    $date_file = intval(preg_replace("/[^0-9]/i", "", $file_name));
    $new_path = $path_file .'/'. date("Y/m/d/", $date_file);
    // create new folder
    if (!is_dir($new_path)) {
        mkdir($new_path, 0777, true);
        chmod($new_path, 0777);
    }
    return $new_path;
}

function generateFileName()
{
    $name = "";
    for ($i = 0; $i < 3; $i++) {
        $name .= chr(rand(97, 122));
    }
    $today = getdate();
    $name .= $today[0];

    return $name;
}

function genOriginPic($file){
    if(!$file) return '';

    $p = generatePathFile('/pictures/origin',$file);
    return $p.'/'.$file;
}

function genCanvasPic($file,$size='140'){
    if(!$file) return '';

    $p = generatePathFile('/pictures/canvas',$file);
    return $p.'/'.$size.'/'.$file;
}

function url_detail($path='',$slug=''){
    if (!$slug) return '/';
    return '/'.$path.'/'.$slug.'.html';
}
function encryptIt($q) {
    $cryptKey  = 'MYOiTVSY0eHhCJRXKtEhiBk';
    $qEncoded  = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return $qEncoded;
}
function decryptIt($q) {
    $cryptKey  = 'MYOiTVSY0eHhCJRXKtEhiBk';
    $qDecoded  = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))),"\0");
    return $qDecoded;
}
function test_curl(){
	$curl = curl_init();
	 curl_setopt_array($curl, array(
	            CURLOPT_URL => "https://api.oss.cdn.i-com.vn/v3/Auth/Token",
	            CURLOPT_RETURNTRANSFER => true,
	            CURLOPT_ENCODING => "",
	            CURLOPT_MAXREDIRS => 10,
	            CURLOPT_TIMEOUT => 30,
	            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	            CURLOPT_CUSTOMREQUEST => "POST",
	            CURLOPT_POSTFIELDS => "Client_id=Authen.Radiome&Client_secret=radiome@Console&DeviceID=1&DeviceToken=1234&Device=Web&DeviceOs=Other&UseRefreshTokens=true&Checksum=25739da5cfe35cf0d95525808645147e",
	            CURLOPT_HTTPHEADER => array(
	                "cache-control: no-cache",
	                "content-type: application/x-www-form-urlencoded"
	            ),
	        ));
	 $response = curl_exec($curl);
	 $err = curl_error($curl);

	 curl_close($curl);

	 if (!$err)
	 {
	      var_dump($response);
	 }
}
function checkPhoneNumber($phone = "", $First = "84")
{
    $vina = array(124, 123, 125, 127, 129, 91, 94, 88 ,81,82,83,84,85);
//    $viettel = array(86, 96, 97, 98, 162, 163, 164, 165, 166, 167, 168, 169);
//    $mobi = array(90, 93, 120, 121, 122, 126, 128, 89);

    $arrFirst = $vina;
    $phone = preg_replace("/[^0-9]/", "", $phone);
    //Loai bo so 0 hoac 84 o dau
    if (substr($phone, 0, 1) == "0") {
        $phone = substr($phone, 1, strlen($phone) - 1);
    }

    if (substr($phone, 0, 2) == "84") {
        $phone = substr($phone, 2, strlen($phone) - 2);
    }

    //Neu ko phai 9 hoac 10  so thi cung ko phai so dien thoai
    if (!(strlen($phone) == 9 || strlen($phone) == 10)) {
        return "";
    }
    if(in_array(substr($phone, 0, 2),$arrFirst) || in_array(substr($phone, 0, 3),$arrFirst)){
        $phone = $First.$phone;
    }else{
        $phone = '';
    }

    return $phone;
}
