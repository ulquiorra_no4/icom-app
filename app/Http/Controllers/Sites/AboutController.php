<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppController;

class AboutController extends AppController
{
    //
    protected $_mact;
    public function __construct(){
        parent::__construct();
        $this->_mact = 1;
    }

    public function index(){
        return view('sites.about',['m_act'=>$this->_mact,'menu'=>$this->_menu]);
    }
}
