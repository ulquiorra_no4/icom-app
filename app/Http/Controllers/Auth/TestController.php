<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;

class TestController extends Controller
{
    //
    use RegistersUsers;
//    use RedirectsUsers;

    protected $redirectTo = '/';

    protected $guard = 'admin';
    public function __construct()
    {
    }

    public function showRegisterForm(){
        return view('cms.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'adm_name' => 'required|string|max:255',
            'adm_username' => 'required|string|min:6',
            'adm_email' => 'required|string|email|max:255|unique:admin_users',
            'adm_password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return Admin::create([
            'adm_name' => $data['name'],
            'adm_username' => $data['username'],
            'adm_email' => $data['email'],
            'adm_password' => bcrypt($data['password']),
        ]);
    }
}
