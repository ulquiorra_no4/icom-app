<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Lang;
use App\Models\Posts;
class RequirementController extends Controller
{

    protected $_limit = 7;
    public function __construct(){

    }

    public function getInfo(){
        $requirement = Lang::get('app.requirement');
        return response()->json($requirement);
    }

    public function getRequirement($page){
        $page = $page ? $page : 1;
        $skip = (($page - 1) * $this->_limit);
        $total = Posts::where(['category_id'=>12])->active()->latest()->count();
        $post = Posts::where(['category_id'=>12])->active()->latest()->skip($skip)->take($this->_limit)->get();
        $a_post = $post->toArray();
        // dd($a_post);
        $map = array_map(function($obj){
            $obj['published_at'] = date('d-m-Y',strtotime($obj['published_at']));
            return $obj;
        },$a_post);
        $post = collect($map);
        return response()->json(['data'=>$post,'total'=>$total]);
    }

    public function getRequirementDetail($params){
        if(!$params) return false;
        $detail = Posts::where(['slug'=>$params])->active()->first();

        return response()->json($detail);
    }
    public function search($keyword){
        if(!$keyword) return null;
        $total = Posts::where(['category_id'=>12])->search($keyword)->active()->latest()->count();
        $post = Posts::where(['category_id'=>12])->search($keyword)->active()->latest()->take($this->_limit)->get();
        return response()->json(['data'=>$post,'total'=>$total]);
    }
}
