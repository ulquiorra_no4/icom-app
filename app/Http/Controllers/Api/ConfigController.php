<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menus\MenuRepository;
use Lang;
use App;
class ConfigController extends Controller
{
    //
    protected $_menu;
    protected $_menuRepository;
    public function __construct(MenuRepository $menuRepository){
        $this->_menuRepository = $menuRepository;
        $this->_menu = [
            'home'=>['title'=>trans('app.menu.home'),'link'=>'/'],
            'us'=>['title'=>trans('app.menu.us'),'link'=>'/about'],
            'service'=>['title'=>trans('app.menu.service'),'link'=>'/services'],
            'news'=>['title'=>trans('app.menu.news'),'link'=>'/news'],
            'requi'=>['title'=>trans('app.menu.requi'),'link'=>'/requirement'],
            'contact'=>['title'=>trans('app.menu.contact'),'link'=>'/contact']
        ];
    }

    public function getMenu(){
        $menu = array_values($this->_menu);
        return response()->json($menu);
    }

    public function getConfigApp($name='app'){
        $get_name = ($name != 'app') ? 'app.'.$name : 'app';
        $config = Lang::get($get_name);
        return response()->json($config);
    }

    public function getMenu2(){
        $menu = $this->_menuRepository->getMenu();
        // $a_menu = $menu->toArray();
        $locale = App::getLocale();
        if($locale=='en'){
            $mn = $menu->map(function($item){
                return [
                    'title'=>$item->menu_name_en,
                    'link'=>$item->menu_link
                ];
            });
        }else{
            $mn = $menu->map(function($item){
                return [
                    'title'=>$item->menu_name,
                    'link'=>$item->menu_link
                ];
            });
        }
        return response()->json($mn);
        // dd($menu);
    }
}
