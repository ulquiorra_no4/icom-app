<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Lang;
use App;
use App\Models\Services;
class ServiceController extends Controller
{
    //
    protected $_a_services;
    protected $_locale = 'vi';
    public function __construct(){

    }

    public function getServices(){
        // $services = Lang::get('app.service');
        $services = Services::active()->take(6)->get();
        $locale = App::getLocale();
        if($locale=='en'){
            $ser = $services->map(function($item){
                return [
                    'id'=>$item->ser_id,
                    'slug'=>$item->slug,
                    'title'=>$item->ser_title_en,
                    'description'=>$item->ser_description_en,
                    'content'=>$item->ser_content_en,
                    'avatar'=>$item->avatar,
                    'image_thumb'=>$item->image_thumb,
                    'created_at'=>$item->created_at
                ];
            });
        }else{
            $ser = $services->map(function($item){
                return [
                    'id'=>$item->ser_id,
                    'slug'=>$item->slug,
                    'title'=>$item->ser_title,
                    'description'=>$item->ser_description,
                    'content'=>$item->ser_content,
                    'avatar'=>$item->avatar,
                    'image_thumb'=>$item->image_thumb,
                    'created_at'=>$item->created_at
                ];
            });
        }

        // dd($ser);
        return response()->json($ser);
    }

    public function getServiceDetail($params){
        if (!$params) {
            return false;
        }
        $detail = [];
        $detail = Services::where(['slug'=>$params])->active()->first();
        $locale = App::getLocale();
        if($locale=='en'){
            $det = collect([
                'id'=>$detail->ser_id,
                'slug'=>$detail->slug,
                'title'=>$detail->ser_title_en,
                'description'=>$detail->ser_description_en,
                'content'=>$detail->ser_content_en,
                'avatar'=>$detail->avatar,
                'image_thumb'=>$detail->image_thumb,
                'created_at'=>$detail->created_at
            ]);
        }else{
            $det = collect([
                'id'=>$detail->ser_id,
                'title'=>$detail->ser_title,
                'description'=>$detail->ser_description,
                'content'=>$detail->ser_content,
                'avatar'=>$detail->avatar,
                'image_thumb'=>$detail->image_thumb,
                'created_at'=>$detail->created_at
                ]);
        }
        return response()->json($det);
    }
}
