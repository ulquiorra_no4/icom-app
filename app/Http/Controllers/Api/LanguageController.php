<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
class LanguageController extends Controller
{
    //
    public function __construct(){

    }
    public function getCurentLocale(){
        $locale = App::getLocale();
        return response()->json($locale);
    }
    public function setLang(Request $request){
        // if($request->ajax()){
            $request->session()->put('locale',$request->locale);
            return response()->json($request->locale);
        // }
    }
}
