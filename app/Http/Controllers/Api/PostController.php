<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Posts;
use Lang;
class PostController extends Controller
{
    //
    protected $_limit = 5;
    public function __construct(){
    }

    public function getInfo(){
        $news = Lang::get('app.news');
        return response()->json($news);
    }
    public function getNews($page){
        $page = $page ? $page : 1;
        $skip = (($page - 1) * $this->_limit);
        $total = Posts::where([['category_id','<>',12]])->active()->latest()->count();
        $post = Posts::where([['category_id','<>',12]])->active()->latest()->skip($skip)->take($this->_limit)->get();
        // Gen link ảnh mới id > 136
        $data = $post->map(function($item){
            $item->thumbnail = $item->id > 136 ? genOriginPic($item->avatar) : '';
            $item->thumbnail140 = $item->id > 136 ? genCanvasPic($item->avatar,140) : '';
            $item->thumbnail200 = $item->id > 136 ? genCanvasPic($item->avatar,200) : '';
            $item->thumbnail300 = $item->id > 136 ? genCanvasPic($item->avatar,300) : '';
            return $item;
        });
        // dd($data);
        return response()->json(['data'=>$data,'total'=>$total]);
    }
    public function getNewsDetail($params){
        if(!$params) return false;
        $detail = [];
        $rel = [];
        $detail = Posts::where(['slug'=>$params])->active()->first();
        if($detail){
            $rel = Posts::where(['category_id'=>$detail->category_id])->active()->latest()->skip(1)->take(5)->get();
            // Gen link ảnh mới id > 136
            $detail->thumbnail = $detail->id > 136 ? genOriginPic($detail->avatar) : '';
            $detail->thumbnail140 = $detail->id > 136 ? genCanvasPic($detail->avatar,140) : '';
            $detail->thumbnail200 = $detail->id > 136 ? genCanvasPic($detail->avatar,200) : '';
            $detail->thumbnail300 = $detail->id > 136 ? genCanvasPic($detail->avatar,300) : '';
            #
            $data_rel = $rel->map(function($item){
                $item->thumbnail = $item->id > 136 ? genOriginPic($item->avatar) : '';
                $item->thumbnail140 = $item->id > 136 ? genCanvasPic($item->avatar,140) : '';
                $item->thumbnail200 = $item->id > 136 ? genCanvasPic($item->avatar,200) : '';
                $item->thumbnail300 = $item->id > 136 ? genCanvasPic($item->avatar,300) : '';
                return $item;
            });
        }

        return response()->json(['detail'=>$detail,'relation'=>$data_rel]);
    }
    public function getSideMenuById(){
        $latest = Posts::where([['category_id','<>',12]])->active()->latest()->skip(1)->take(5)->get();
        $view = Posts::where([['category_id','<>',12]])->active()->orderBy('view', 'desc')->skip(1)->take(5)->get();
        return response()->json(['latest'=>$latest,'view'=>$view]);
    }
    public function search($keyword,$page){
        if(!$keyword) return null;
        $page = $page ? $page : 1;
        $skip = (($page - 1) * $this->_limit);
        $total = Posts::where([['category_id','<>',12]])->search($keyword)->active()->latest()->count();
        $post = Posts::where([['category_id','<>',12]])->search($keyword)->active()->latest()->skip($skip)->take($this->_limit)->get();
        // Gen link ảnh mới id > 136
        $data = $post->map(function($item){
            $item->thumbnail = $item->id > 136 ? genOriginPic($item->avatar) : '';
            $item->thumbnail140 = $item->id > 136 ? genCanvasPic($item->avatar,140) : '';
            $item->thumbnail200 = $item->id > 136 ? genCanvasPic($item->avatar,200) : '';
            $item->thumbnail300 = $item->id > 136 ? genCanvasPic($item->avatar,300) : '';
            return $item;
        });
        return response()->json(['data'=>$data,'total'=>$total]);
    }
}
