<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Modules\ModuleRepository;
use App\Http\Requests\ModuleRequest;

class ModuleController extends Controller
{
    //
    protected $_o_module;
    protected $head_title;
    protected $_table_head;

    public function __construct(ModuleRepository $modules)
    {
        $this->_o_module = $modules;
        $this->head_title = trans('table.module.name');
        $this->_table_head = $this->table_head();
    }

    public function index()
    {
        $modules = $this->_o_module->fetchAll();
        return view('cms.modules.index', ['modules' => $modules, 'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show form create a new module
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cms.modules.add');
    }

    /**
     * Store a new module
     * @param ModuleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ModuleRequest $request)
    {
        $data = $this->attribute($request->all());
//        dd($data);
        $this->_o_module->save($data);
        \Session::flash('alert_success', trans('alert.success.add'));
        return redirect()->intended('/admin/modules/add');
    }

    /**
     * Get a module by id to edit form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $module = $this->_o_module->findById($id);

        return view('cms.modules.edit', ['module' => $module]);
//        dd($module->mod_name);
    }

    /**
     * Update a module by id
     * @param $id
     * @param ModuleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, ModuleRequest $request)
    {
        $data = $this->attribute($request->all());
        $data['mod_id'] = (int)$id;
//        dd($data);
        $this->_o_module->save($data);
        \Session::flash('alert_success', trans('alert.success.update'));
        return redirect()->route('mod');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $id = (int)$id;
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_module->destroy($id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }

    /**
     * Delete multi item select
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_module->multiDestroy($a_id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }

    /**
     * Create a data form
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'mod_code' => $data['mod_code'],
            'mod_name' => $data['mod_name'],
            'mod_name_en' => $data['mod_name_en'],
            'mod_alias' => $data['mod_alias'],
            'mod_description' => $data['mod_description'],
            'order' => (int)$data['order'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }

    /**
     * create head of table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'mod_code',
                'name' => trans('label.module.code'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'mod_name',
                'name' => trans('label.module.name'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'mod_name_en',
                'name' => trans('label.module.nameEn'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'mod_alias',
                'name' => trans('label.module.alias'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.module.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'order',
                'name' => trans('label.module.order'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'updated_at',
                'name' => trans('label.updated_at'),
                'options' => [
                    'icon' => ''
                ],
            ],

        ];
    }
}
