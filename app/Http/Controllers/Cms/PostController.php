<?php

namespace App\Http\Controllers\Cms;

use App\Models\Categories\CategoryRepository;
use App\Models\Posts\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Posts\Posts;
use Illuminate\Support\Facades\Auth;
use Image;
use File;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    protected $_o_post;
    protected $_o_category;
    protected $head_title;
    protected $_table_head;
    protected $_page = 1;
    protected $_pageSize = 10;
    public function __construct(PostRepository $postRepository, CategoryRepository $categoryRepository)
    {
        $this->_o_post = $postRepository;
        $this->_o_category = $categoryRepository;
        $this->head_title = trans('table.post');
        $this->_table_head = $this->table_head();
    }

    public function index(Request $request)
    {
        $keyword = $request->get('keyword');
        // dd($request->get('keyword'));
        $where = [];
        if($keyword){
            $posts = $this->_o_post->searchPost($keyword,$where,$this->_pageSize);
        }else {
            $posts = $this->_o_post->getList($where,$this->_pageSize);
        }
        // $posts = $this->_o_post->fetchAll();

        $posts->withPath('/admin/posts');
        // dd($posts);
        return view('cms.posts.index',['posts'=>$posts,'head_title' => $this->head_title, 'table_head' => $this->_table_head,'keyword'=>$keyword]);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->can('create',Posts::class)){
            $a_categories = $this->_o_category->getPluckCategory();
            return view('cms.posts.add', ['a_categories' => $a_categories]);
        }
        return redirect()->back();
    }
    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $this->attribute($request->all());
        // dd($data);
        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            $canvasPath = public_path(generatePathFile(config('picture.canvasPath'),$data['avatar']));
            // Create folder if not exit
            File::makeDirectory($canvasPath.'140/', 0775, true, true);
            File::makeDirectory($canvasPath.'200/', 0775, true, true);
            File::makeDirectory($canvasPath.'300/', 0775, true, true);
            // Create thumbnail
            $img = Image::make($image->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'200/'.$data['avatar']);
            $img->resize(140, 140, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'140/'.$data['avatar']);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'300/'.$data['avatar']);
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        $data['stage'] = 'draft';
        $data['author_id'] = 1;
        // dd($data);
        if($this->_o_post->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->intended('/admin/posts/add');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $post = $this->_o_post->findById($id);
        if (!$post) return redirect()->route('pos');
        if($user->can('update',$post)){
            $a_categories = $this->_o_category->getPluckCategory();
            // dd($post);
            return view('cms.posts.edit',['post'=>$post,'a_categories' => $a_categories]);
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $data = $this->attribute($request->all());
        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            $canvasPath = public_path(generatePathFile(config('picture.canvasPath'),$data['avatar']));
            // Create folder if not exit
            File::makeDirectory($canvasPath.'140/', 0775, true, true);
            File::makeDirectory($canvasPath.'200/', 0775, true, true);
            File::makeDirectory($canvasPath.'300/', 0775, true, true);
            // Create thumbnail
            $img = Image::make($image->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'200/'.$data['avatar']);
            $img->resize(140, 140, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'140/'.$data['avatar']);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'300/'.$data['avatar']);
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        $data['id'] = $id;
        // dd($data);
        if($this->_o_post->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->route('pos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id, Request $request)
     {
         $user = Auth::user();
         $id = (int)$id;
         $error = 1;
         $mess = '';
         if($user->can('delete',Posts::class)){
             if ($request->ajax()) {
                 if ($this->_o_post->destroy($id)) {
                     $error = 0;
                     $mess = trans('alert.success.delete');
                 } else {
                     $error = 1;
                     $mess = trans('alert.error.delete');
                 }
             }
             $return = [
                 'error' => $error,
                 'mess' => $mess
             ];
             return response()->json($return);
         }
     }

     /**
      * Delete multi item
      * @param Request $request
      * @return \Illuminate\Http\JsonResponse
      */
     public function multi_destroy(Request $request)
     {
         $user = Auth::user();

         $a_data = $request->only('id');
         $a_id = $a_data['id'];
         $error = 1;
         $mess = '';
         if($user->can('delete',Posts::class)){
             if ($request->ajax()) {
                 if ($this->_o_post->multiDestroy($a_id)) {
                     $error = 0;
                     $mess = trans('alert.success.delete');
                 } else {
                     $error = 1;
                     $mess = trans('alert.error.delete');
                 }
             }
             $return = [
                 'error' => $error,
                 'mess' => $mess
             ];
             return response()->json($return);
         }
     }

    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'title' => $data['title'],
            'title_en' => $data['title_en'],
            'slug' => str_slug($data['title'],'-'),
            'description' => $data['description'],
            'description_en' => $data['description_en'],
            'content' => $data['content'],
            'content_en' => $data['content_en'],
            'keywords' => $data['keywords'],
            'category_id' => (int)$data['category_id'],
            'tags' => $data['tags'],
            'active_time' => $data['active_time'],
            'option' => $data['option'],
            'salary' => $data['salary'],
            'place' => $data['place'],
            'position' => $data['position'],
            'end_time' => $data['end_time'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }

    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'title',
                'name' => trans('label.post.title'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'category',
                'name' => trans('label.post.catId'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.post.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'updated_at',
                'name' => trans('label.updated_at'),
                'options' => [
                    'icon' => ''
                ],
            ]
        ];
    }
}
