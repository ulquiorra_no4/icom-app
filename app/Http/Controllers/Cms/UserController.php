<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserRequest;
use App\Models\Users\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Image;
use File;
class UserController extends Controller
{
    private $_role = ['member'=>'member','administrator'=>'administrator','admin'=>'admin','poster'=>'poster'];
    protected $head_title;
    protected $_table_head;
    public function __construct(){
        $this->head_title = trans('table.user.name');
        $this->_table_head = $this->table_head();
        // dd(Auth::guard('admin')->user()->password);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->can('view',Admin::class)){
            $users = Admin::all();
            // dd($users);
            return view('cms.users.index',['users'=>$users,'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            $a_role = $this->_role;
            return view('cms.users.add',['a_role'=>$a_role]);
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserRequest $request)
    {
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            // dd($this->attribute($request->all()));
            Admin::create($this->attribute($request->all()));
            return redirect()->intended('/admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::guard('admin')->user();
        return view('cms.users.profile',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            $user = Admin::findOrFail($id);
            $a_role = $this->_role;
            // dd($user);
            return view('cms.users.edit',['user'=>$user,'a_role'=>$a_role]);
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            // dd($this->attribute($request->all()));
            $data = $this->attribute($request->all());
            $insert = array_filter($data,'strlen');
            // dd($insert);
            Admin::where('id',$id)->update($insert);
            return redirect()->intended('/admin/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $id = (int)$id;
        $error = 1;
        $mess = '';
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            if ($request->ajax()) {
                $user = Admin::findOrFail($id);
                if ($user->delete()) {
                    $error = 0;
                    $mess = trans('alert.success.delete');
                } else {
                    $error = 1;
                    $mess = trans('alert.error.delete');
                }
            }
            $return = [
                'error' => $error,
                'mess' => $mess
            ];
            return response()->json($return);
        }
    }
    /**
     * Delete multi item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        $user = Auth::user();
        if($user->can('right',Admin::class)){
            if ($request->ajax()) {
                if ($a_id) {
                    // Admin::destroy($a_id);
                    $error = 0;
                    $mess = trans('alert.success.delete');
                } else {
                    $error = 1;
                    $mess = trans('alert.error.delete');
                }
            }
            $return = [
                'error' => $error,
                'mess' => $mess
            ];
            return response()->json($return);
        }
    }
    /**
    *
    */
    public function update_profile(Request $request){
        $error = 1;
        $mess = '';
        $user = Auth::user();
        if($user->can('update',Admin::class)){
            if($request->ajax()){
                return response()->json($request->all());
            }
        }
    }
    public function update_avatar(Request $request){
        $error = 1;
        $mess = '';
        $source = '';
        $user = Auth::user();
        if($user->can('update',Admin::class)){
            $id = Auth::guard('admin')->user()->id;
            if(!$id) return false;
            if($request->ajax()){
                if ($request->hasFile('avatar')) {
                    $image = $request->file('avatar');

                    $avatar = generateFileName() . '.' . $image->getClientOriginalExtension();
                    $originPath = public_path(generatePathFile(config('picture.originPath'),$avatar));
                    $canvasPath = public_path(generatePathFile(config('picture.canvasPath'),$avatar));
                    // Create folder if not exit
                    File::makeDirectory($canvasPath.'140/', 0775, true, true);
                    File::makeDirectory($canvasPath.'200/', 0775, true, true);
                    File::makeDirectory($canvasPath.'300/', 0775, true, true);
                    // Create thumbnail
                    $img = Image::make($image->getRealPath());
                    $img->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();

                    })->save($canvasPath.'200/'.$avatar);
                    $img->resize(140, 140, function ($constraint) {
                        $constraint->aspectRatio();

                    })->save($canvasPath.'140/'.$avatar);
                    $img->resize(300, 300, function ($constraint) {
                        $constraint->aspectRatio();

                    })->save($canvasPath.'300/'.$avatar);
                    // Create image origin
                    $image->move($originPath, $avatar);
                    Auth::guard('admin')->user()->avatar = $avatar;
                    Auth::guard('admin')->user()->save();
                    $error = 0;
                    $mess = 'Upload successfull';
                    $source = genOriginPic($avatar);
                }
            }
            $return = [
                'error' => $error,
                'mess' => $mess,
                'source' => $source
            ];
            return response()->json($return);
        }
    }
    public function update_security(Request $request){
        // dd($request->only('password'));
        $messages = [
            'old_password.required'=>'Vui lòng nhập mật khẩu hiện tại.',
            'old_password.old_password'=>'Mật khẩu không đúng.',
            'password.required'=>'Vui lòng nhập mật khẩu mới.',
            'password.min' => 'Mật khẩu chứa ít nhất 6 kí tự.',
            'password.confirmed' => 'Xác nhận mật khẩu mới không khớp.'
        ];
        $user = Auth::user();
        if($user->can('update',Admin::class)){
            $validator = Validator::make($request->all(), [
                'old_password' => 'required|old_password',
                'password' => 'required|min:6|confirmed',
            ],$messages);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }else {
                Auth::guard('admin')->user()->password = bcrypt($request->only('password')['password']);
                Auth::guard('admin')->user()->save();
                return redirect()->back()->withSuccess('Cập nhật mật khẩu thành công !.');
            }
        }
    }
    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'role' => $data['role'],
            'address' => $data['address'],
            'password' => $data['password']?bcrypt($data['password']):'',
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }
    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'name',
                'name' => trans('label.user.name'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'email',
                'name' => trans('label.user.email'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'role',
                'name' => trans('label.user.role'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.user.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ]
        ];
    }
}
