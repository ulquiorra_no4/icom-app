<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Routing\Middleware;
use Session;
use App;
use Config;
class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        // Make sure current locale exists.
        if(Session::has('locale')){
            $locale = Session::get('locale',Config::get('app.locale'));
        }else{
            $locale = 'vi';
        }
        App::setLocale($locale);
        return $next($request);
    }


}
