<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'menu_name'=>'required|unique:menuses,menu_id'
        ];
    }

    public function messages(){
        return [
            'menu_name'=>[
                'required'=>trans('validation.required'),
                'unique'=>trans('validation.unique')
            ]
        ];
    }
}
