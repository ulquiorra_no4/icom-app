<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class AdminUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:50|',
            'username'=>'required|max:50|min:6|unique:admin_users,id',
            'email'=>'required|email|max:100|unique:admin_users,id',
            'role'=>[
                'required',
                Rule::in(['member','administrator','admin','poster'])
            ],
            'password'=>'required|min:6|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'name' => [
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
            ],
            'username'=>[
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique'),
                'min' => trans('validation.min.string')
            ],
            'email'=>[
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique')
            ],
            'role'=>[
                'required' => trans('validation.required'),
                'in' => trans('validation.in'),
            ],
            'password' => [
                'required'=> trans('validation.required'),
                'min' => trans('validation.min.string'),
                'confirmed'=>trans('validation.confirmed')
            ]
        ];
    }
}
