<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ser_title'=>'required|max:255|unique:services,ser_id',
            'ser_title_en'=>'required|max:255',
            'ser_avatar'=>'image|mimes:jpg,jpeg,png,gif,svg|max:3072',
            'ser_thumbnail'=>'image|mimes:jpg,jpeg,png,gif,svg|max:3072',
        ];
    }

    public function messages(){
        return [
            'ser_title'=>[
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique')
            ],
            'ser_title_en'=>[
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string')
            ],
            'ser_avatar' => [
                'image' => trans('validation.image'),
                'max' => trans('validation.max.file'),
                'mimes' => trans('validation.mimes')
            ],
            'ser_thumbnail' => [
                'image' => trans('validation.image'),
                'max' => trans('validation.max.file'),
                'mimes' => trans('validation.mimes')
            ],
        ];
    }
}
