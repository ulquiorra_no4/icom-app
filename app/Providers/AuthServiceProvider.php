<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\PostPolicy;
use App\Policies\ServicePolicy;
use App\Policies\InfoSitesPolicy;
use App\Policies\MenusPolicy;
use App\Policies\AdminPolicy;
use App\Models\Services\Services;
use App\Models\InfoSites\InfoSites;
use App\Models\Menus\Menus;
use App\Models\Posts\Posts;
use App\Models\Users\Admin;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Posts::class => PostPolicy::class,
        Services::class => ServicePolicy::class,
        InfoSites::class => InfoSitesPolicy::class,
        Menus::class => MenusPolicy::class,
        Admin::class => AdminPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
